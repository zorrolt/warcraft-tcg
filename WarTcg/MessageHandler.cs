﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WarTcg
{
    public sealed class MessageHandler
    {
        private readonly Dictionary<string, List<EventHandler>> _events;

        private MessageHandler()
        {
            _events = new Dictionary<string, List<EventHandler>>();
        }

        private static MessageHandler _mh;

        private static MessageHandler Instance => _mh ?? (_mh = new MessageHandler());

        public static void RegisterMessage(string name)
        {
            if (!Instance._events.ContainsKey(name))
                Instance._events.Add(name, new List<EventHandler>());
        }

        public static void SubscribeMessage(string name, EventHandler callBack)
        {
            if (Instance._events.ContainsKey(name))
                Instance._events[name].Add(callBack);
        }

        public static async void SendMessageAsync(string name, object sender = null, EventArgs args = null)
        {
            if (Instance._events.ContainsKey(name))
                foreach (var callback in Instance._events[name])
                    await Task.Run(() => callback?.Invoke(sender, args ?? EventArgs.Empty));
        }
        public static void SendMessage(string name, object sender = null, EventArgs args = null)
        {
            if (Instance._events.ContainsKey(name))
                foreach (var callback in Instance._events[name])
                    callback?.Invoke(sender, args ?? EventArgs.Empty);
        }

        public static void UnsubscribeMessage(string name, EventHandler callBack)
        {
            if (Instance._events.ContainsKey(name))
                Instance._events[name].Remove(callBack);
        }
    }
}