﻿using EngineWar;
using Microsoft.Xna.Framework;

namespace WarTcg
{
    public static class GlobalVariables
    {
        // Screens
        public const string MenuScreenName = "Menu";
        public const string GameScreenName = "Game";

        // Data files
        public const string MenuContentFile = "Data/MenuContent.json";
        public const string GameContentFile = "Data/GameContent.json";
        public const string DecksFile = "Data/Decks.json";
        public const string CardsFile = "Data/Cards.json";
        public const string DeckCardsPath = "Data/Decks/";

        // Content 
        public const string DefaultCardsBack = "cardback";
        public const string DefaultCardHighlight = "CardHighlight";
        public const string LoadingScreenBackground = "LoadingBackground";
        public const string ButtonContentName = "Button";
        public const string LoadingBarContentName = "LoadingBar";
        public const string MenuFontName = "Menu";
        public const string BoardFontName = "Board";
        public const string HordeButtonContentName = "HordeCrest";
        public const string AllianceButtonContentName = "AllianceCrest";
        public const string PickDeckBackground = "DemoPickDeckBackground";
        public const string CardsContentFolder = "Game/Cards/";
        public const string NormalBackgroundName = "Tabletop";
        public const string BattleBackgroundName = "TabletopB";
        public const string HealthIcon = "HealthIco";
        public const string WeaponIcon = "WeaponIco";
        public const string ArmorIcon = "ArmorIco";
        public const string LostBackground = "LostBackground";
        public const string WonBackground = "WonBackground";
        public const string CheckBoxContentName = "Checkbox";
        public const string MeleIcon = "MeleIco";

        // Scenes
        public const string MainMenuSceneName = "Main";
        public const string GameOverScene = "Over";
        public const string MainGameScene = "Main";
        public const string PickDeckSceneName = "Pick";
        public const string InGameMenuSceneName = "InGameMenu";
        public const string LoadingSceneName = "Loading";
        public const string SettingsSceneName = "Settings";

        // Game const
        public const int HandCardsCount = 7;
        public static readonly Point CardZoomedDim = new Point(375, 528);
        public static readonly int DeckSpace = 40;
        public const int SpacesCard = 20;

        /// <summary>
        /// Use only after Initialize !
        /// </summary>
        public static Point BoardCardDim
        {
            get
            {
                var y = (ScreenManager.Instance.Engine.GetResolution().Y - MenuButtonsSize.Y) / 6;
                var x = (int)(y * 0.71);
                return new Point(x, y);
            }
        }

        /// <summary>
        /// Use only after Initialize !
        /// </summary>
        public static Point HandCardDim
        {
            get
            {
                var y = ScreenManager.Instance.Engine.GetResolution().Y / 4;
                var x = (int)(y * 0.71);
                return new Point(x, y);
            }
        }

        public static Rectangle P1ResourceZoneDim => new Rectangle(DeckSpace + BoardCardDim.X, EngineWar.ScreenManager.Instance.Engine.GetResolution().Y - BoardCardDim.Y, EngineWar.ScreenManager.Instance.Engine.GetResolution().X - DeckSpace - HandCardDim.X, BoardCardDim.Y);
        public static Rectangle P1HeroZoneDim => new Rectangle(DeckSpace + HandCardDim.X, EngineWar.ScreenManager.Instance.Engine.GetResolution().Y - BoardCardDim.Y * 2, EngineWar.ScreenManager.Instance.Engine.GetResolution().X - DeckSpace - HandCardDim.X, BoardCardDim.Y);
        public static Rectangle P1AllyZoneDim => new Rectangle(DeckSpace + HandCardDim.X, EngineWar.ScreenManager.Instance.Engine.GetResolution().Y - BoardCardDim.Y * 3, EngineWar.ScreenManager.Instance.Engine.GetResolution().X - DeckSpace - HandCardDim.X, BoardCardDim.Y);
        public static Rectangle P2ResourceZoneDim => new Rectangle(0, 0, EngineWar.ScreenManager.Instance.Engine.GetResolution().X - DeckSpace - HandCardDim.X, BoardCardDim.Y);
        public static Rectangle P2HeroZoneDim => new Rectangle(0, BoardCardDim.Y, EngineWar.ScreenManager.Instance.Engine.GetResolution().X - DeckSpace, BoardCardDim.Y);
        public static Rectangle P2AllyZoneDim => new Rectangle(0, BoardCardDim.Y * 2, EngineWar.ScreenManager.Instance.Engine.GetResolution().X - DeckSpace - HandCardDim.X, BoardCardDim.Y);

        public static int HandOffset => EngineWar.ScreenManager.Instance.Engine.GetResolution().X/4;
        public const int HandOffsetS = 4;
        public const int HandMaxSpace = 20;
        public const int BoardMaxSpace = 25;
        public static readonly Point EndTurnButtonDim = new Point(150, 50);
        public const int ZoomCardMulti = 3;
        public static readonly Point HealthIcoSize = new Point(25, 40);
        public static readonly Point CardHealthTextboxSize = HealthIcoSize + new Point(10, 5);
        public static readonly Point ArmorIcoSize = new Point(32, 30);
        public static readonly Point WeaponIcoSize = new Point(29, 29);
        public static readonly Point CardAttackTextboxSize = WeaponIcoSize + new Point(15, 8);
        public static readonly Point LoadingBarSize = new Point(1090, 46);
        public static readonly Point MenuButtonsSize = new Point(300, 100);
        public static readonly Point CheckBoxSize = new Point(40, 40);
        public static readonly Point DropdownSize = new Point(500, 60);

        public const int DamageToWeapon = 1;

        // TurnPhase Messages
        public const string TurnStarted = "TurnStarted";
        public const string ActionStarted = "ActionStarted";
        public const string TurnEnded = "TurnEnded";
        public const string BattleStarted = "BattleStarted";
        public const string BattleEnded = "BattleEnded";

        // Game events
        public const string Discard = "Discard";
        public const string Discarded = "Discarded";
        public const string EndTurn = "EndTurn";
        public const string CardClicked = "CardClicked";
        public const string CardRightClicked = "CardRightClicked";
        public const string CardHover = "CardHover";
        public const string CardLeave = "CardLeave";
        public const string CardDied = "CardDied";
        public const string CardZoomed = "CardZoomed";
        public const string CardZoomedOut = "CardZoomedOut";
        public const string HeroDied = "HeroDied";
        public const string GameEnded = "GameEnded";
        public const string ShowMessage = "ShowMessage";
        public const string HideMessage = "HideMessage";
        public const string MulliganMessage = "Mulligan";
        public const string EnableEndTurn = "EnableEndTurn";

        //Menu events
        public const string ToMainMenu = "ToMain";
        public const string ToPauseMenu = "ToPause";
        public const string ToSettingsMenu = "ToSettings";
        public const string GameIsLoaded = "Loaded";
        public const string NewGame = "NewGame";
        public const string StartGame = "StartGame";
        public const string SetData = "SetData";
        public const string NextTurn = "NextTurn";

        public enum Turn
        {
            Player2, Player
        }
    }
}
