﻿using System;
using System.Collections.Generic;
using System.Linq;
using EngineWar;
using EngineWar.DataEntities;
using EngineWar.GUI;
using Microsoft.Xna.Framework;
using WarTcg.DataEntities;
using WarTcg.DataEntities.DeckData;
using WarTcg.Game;
using WarTcg.Game.Cards;

namespace WarTcg.Menu
{
    internal class LoadingScene : Scene
    {
        private ProgressBar _loadingBar;
        private readonly DataReader _dataReader;
        private readonly Random _rand = new Random();
        private readonly CardsData _cards;
        private Picture _background;

        public LoadingScene() : base(GlobalVariables.LoadingSceneName)
        {
            _dataReader = new DataReader();
            _dataReader.OpenFile(GlobalVariables.CardsFile);
            _cards = _dataReader.Get<CardsData>();
            _dataReader.Close();
        }

        public override void Initialize()
        {
            _background = new Picture(GlobalVariables.LoadingScreenBackground);
            EngineWar.MessageHandler.Instance.SubscribeMessage(EngineWar.GlobalVariables.ResolutionChangedMessage,
                (sender, args) =>
                    _background.Dimension =
                        new Rectangle(new Point(), EngineWar.ScreenManager.Instance.Engine.GetResolution()));
            _loadingBar =
                new ProgressBar(
                    new Rectangle(
                        new Point(
                            (ScreenManager.Instance.Engine.GetResolution().X - GlobalVariables.LoadingBarSize.X) / 2,
                            1000), GlobalVariables.LoadingBarSize),
                    GlobalVariables.LoadingBarContentName, GlobalVariables.MenuFontName);
            AddElement(Layers.Second, _background);
            AddElement(Layers.Top, _loadingBar);
        }

        private void LoadDeckData(DeckCardsData data, List<Content.ElementData> content, List<Card> deck)
        {
            LoadCardsData<AllyCard>(data.Allies, content, deck);
            LoadCardsData<AbilityCard>(data.Abilities, content, deck);
            LoadCardsData<QuestCard>(data.Quests, content, deck);
            LoadCardsData<WeaponCard>(data.Items.Weapons, content, deck);
            LoadCardsData<ArmorCard>(data.Items.Armors, content, deck);
        }

        private void LoadCardsData<T>(List<DeckCardData> data, List<Content.ElementData> content, List<Card> deck) where T: Card
        {
            foreach (var card in data)
            {
                if (!content.Exists(o => o.Name == card.ContentName))
                    content.Add(new Content.ElementData
                    {
                        Name = card.ContentName,
                        Path = GlobalVariables.CardsContentFolder + card.ContentName
                    });
                for (int i = 0; i < card.Count; i++)
                    deck.Add(CreateCard<T>(card.ContentName));
            }
        }
        
        private Card CreateCard<T>(string contentName) where T : Card
        {
            var picture = new Picture(contentName) { Show = false };
            if (typeof(T) == typeof(AllyCard))
                return new AllyCard(picture, GlobalVariables.DefaultCardsBack, _cards.Allies.First(o => o.ContentName == contentName));
            if (typeof(T) == typeof(AbilityCard))
                return new AbilityCard(picture, GlobalVariables.DefaultCardsBack, _cards.Abilities.First(o => o.ContentName == contentName));
            if (typeof(T) == typeof(QuestCard))
                return new QuestCard(picture, GlobalVariables.DefaultCardsBack, _cards.Quests.First(o => o.ContentName == contentName));
            if (typeof(T) == typeof(WeaponCard))
                return new WeaponCard(picture, GlobalVariables.DefaultCardsBack, _cards.Items.Weapons.First(o => o.ContentName == contentName));
            if (typeof(T) == typeof(ArmorCard))
                return new ArmorCard(picture, GlobalVariables.DefaultCardsBack, _cards.Items.Armors.First(o => o.ContentName == contentName));
            throw new Exception("Unsupported card type");
        }

        public void LoadGame(DeckData playerDeckName, DeckData enemyDeckName)
        {
            _dataReader.OpenFile(GlobalVariables.DeckCardsPath + enemyDeckName.File);
            var enemyDeckCardsData = _dataReader.Get<DeckCardsData>();
            _dataReader.Close();
            _dataReader.OpenFile(GlobalVariables.DeckCardsPath + playerDeckName.File);
            var playerDeckCardsData = _dataReader.Get<DeckCardsData>();
            _dataReader.Close();
            var content = new List<Content.ElementData>
            {
                new Content.ElementData
                {
                    Name = GlobalVariables.DefaultCardsBack,
                    Path = GlobalVariables.CardsContentFolder + GlobalVariables.DefaultCardsBack
                }
            };

            // ENEMY DATA
            var enemyDeck = new List<Card>(enemyDeckName.TotalCards);
            content.Add(new Content.ElementData
            {
                Name = enemyDeckName.Hero,
                Path = GlobalVariables.CardsContentFolder + enemyDeckName.Hero
            });
            content.Add(new Content.ElementData
            {
                Name = _cards.Heroes.First(o => o.ContentName == enemyDeckName.Hero).BackContentName,
                Path = GlobalVariables.CardsContentFolder + enemyDeckName.Hero
            });
            var enemyHero = new HeroCard(new Picture(enemyDeckName.Hero) {Show = false},
                _cards.Heroes.First(o => o.ContentName == enemyDeckName.Hero).BackContentName,
                _cards.Heroes.First(o => o.ContentName == enemyDeckName.Hero));
            LoadDeckData(enemyDeckCardsData, content, enemyDeck);

            // PLAYER DATA
            var playerDeck = new List<Card>(enemyDeckName.TotalCards);
            content.Add(new Content.ElementData
            {
                Name = playerDeckName.Hero,
                Path = GlobalVariables.CardsContentFolder + playerDeckName.Hero
            });
            content.Add(new Content.ElementData
            {
                Name = _cards.Heroes.First(o => o.ContentName == playerDeckName.Hero).BackContentName,
                Path = GlobalVariables.CardsContentFolder + playerDeckName.Hero
            });
            var playerHero = new HeroCard(new Picture(playerDeckName.Hero) {Show = false},
                _cards.Heroes.First(o => o.ContentName == playerDeckName.Hero).BackContentName,
                _cards.Heroes.First(o => o.ContentName == playerDeckName.Hero));
            LoadDeckData(playerDeckCardsData, content, playerDeck);

            var start = (GlobalVariables.Turn) _rand.Next(2);

            MessageHandler.SendMessage(GlobalVariables.GameIsLoaded, this, new GameScreen.SetGameArgs
            {
                Content =  content,
                EnemyDeck = enemyDeck,
                EnemyHero = enemyHero,
                First = start,
                PlayerDeck = playerDeck,
                PlayerHero = playerHero
            });
            _loadingBar.Set(0);
            _loadingBar.OnLoad =
                () =>
                {
                    var gameScreen = ScreenManager.Instance.GetScreen(GlobalVariables.GameScreenName) as GameScreen;
                    if (gameScreen != null)
                        _loadingBar.Set(
                            gameScreen.Progress);
                };
            _loadingBar.OnDone = () =>
            {
                ScreenManager.Instance.SetScreen(GlobalVariables.GameScreenName);
            };
            ScreenManager.Instance.LoadScreen(GlobalVariables.GameScreenName);
        }
    }
}
