﻿using System.Collections.Generic;
using EngineWar;
using EngineWar.GUI;
using Microsoft.Xna.Framework;

namespace WarTcg.Menu
{
    internal class SettingsMenu : Scene
    {
        private TextBox _isFullscreenLabel, _resolutionLabel;
        private CheckBox _isFullscreen;
        private Dropdown _dropdown;
        private Button _backB, _applyB;
        private Point _posBb, _posAb;

        private const int LabelsPosX = 50;
        private const int SettingsBaseY = 100;
        private const int LabelLength = 300;
        private const int SettingsX = LabelsPosX + LabelLength;
        private const int LabelHeight = 60;

        //TODO: fix default resolutions problem
        //TODO: use supported resolutions
        //TODO: use smaller font for labels
        public SettingsMenu() : base(GlobalVariables.SettingsSceneName)
        {
            
        }

        public override void Initialize()
        {
            EngineWar.MessageHandler.Instance.SubscribeMessage(EngineWar.GlobalVariables.ResolutionChangedMessage, (sender, args) => RearrangeUi());
            _posBb = new Point(ScreenManager.Instance.Engine.GetResolution().X,
                         ScreenManager.Instance.Engine.GetResolution().Y) - new Point(0, 150);
            _posBb.X = 100;
            _posAb = new Point(ScreenManager.Instance.Engine.GetResolution().X,
                    ScreenManager.Instance.Engine.GetResolution().Y) -
                new Point(100 + GlobalVariables.MenuButtonsSize.X, 150);

            _isFullscreenLabel = new TextBox(new Rectangle(LabelsPosX, SettingsBaseY, LabelLength, LabelHeight), "Is fullscreen:")
            {
                FontName = GlobalVariables.MenuFontName,
                FontColor = Color.Black
            };
            _isFullscreenLabel.SetBackgroundColor(Color.Transparent);
            _backB = new Button(new Rectangle(_posBb, GlobalVariables.MenuButtonsSize),
                () => MessageHandler.SendMessage(GlobalVariables.ToMainMenu), GlobalVariables.ButtonContentName,
                GlobalVariables.MenuFontName)
            { Text = "Back" };
            _isFullscreen = new CheckBox(new Rectangle(new Point(SettingsX, SettingsBaseY + (LabelHeight - GlobalVariables.CheckBoxSize.Y)), GlobalVariables.CheckBoxSize), GlobalVariables.CheckBoxContentName);
            _isFullscreen.SetSelect(ScreenManager.Instance.Engine.IsFullScreen());
            //var supported = ScreenManager.Instance.Engine.GetSupportedResolutions();
            var resolution = ScreenManager.Instance.Engine.GetResolution();
            _resolutionLabel = new TextBox(new Rectangle(LabelsPosX, SettingsBaseY + LabelHeight + LabelsPosX, LabelLength, LabelHeight), "Resolution:")
            {
                FontName = GlobalVariables.MenuFontName,
                FontColor = Color.Black
            };
            var res = new Dictionary<string, string>
            {
                {"1024X768", "1024X768"},
                {"1280X800", "1280X800"},
                {"1366X768", "1366X768"},
                {"1920X1080", "1920X1080"},
            };
            _resolutionLabel.SetBackgroundColor(Color.Transparent);
            _dropdown = new Dropdown(res,
                new Rectangle(new Point(SettingsX, SettingsBaseY + LabelHeight + LabelsPosX + (LabelHeight - GlobalVariables.DropdownSize.Y)), GlobalVariables.DropdownSize), font: GlobalVariables.MenuFontName);
            _dropdown.Select(resolution.X + "X" + resolution.Y);
            _applyB = new Button(new Rectangle(_posAb, GlobalVariables.MenuButtonsSize),
                () =>
                {
                    ScreenManager.Instance.Engine.ChangeResolution(_dropdown.Selected, _isFullscreen.IsSelected);
                }, GlobalVariables.ButtonContentName,
                GlobalVariables.MenuFontName)
            { Text = "Apply" };
            AddElement(Layers.Top, _backB);
            AddElement(Layers.Top, _isFullscreen);
            AddElement(Layers.Top, _applyB);
            AddElement(Layers.Top, _dropdown);
            AddElement(Layers.Top, _isFullscreenLabel);
            AddElement(Layers.Top, _resolutionLabel);
        }

        private void RearrangeUi()
        {
            _posBb = new Point(ScreenManager.Instance.Engine.GetResolution().X,
                         ScreenManager.Instance.Engine.GetResolution().Y) - new Point(0, 150);
            _posBb.X = 100;
            _posAb = new Point(ScreenManager.Instance.Engine.GetResolution().X,
                    ScreenManager.Instance.Engine.GetResolution().Y) -
                new Point(100 + GlobalVariables.MenuButtonsSize.X, 150);
            _isFullscreenLabel.Dimension = new Rectangle(LabelsPosX, SettingsBaseY, LabelLength, LabelHeight);
            _isFullscreen.Dimension =
                new Rectangle(new Point(SettingsX, SettingsBaseY + (LabelHeight - GlobalVariables.CheckBoxSize.Y)),
                    GlobalVariables.CheckBoxSize);
            _isFullscreen.SetSelect(ScreenManager.Instance.Engine.IsFullScreen());
            _resolutionLabel.Dimension = new Rectangle(LabelsPosX, SettingsBaseY + LabelHeight + LabelsPosX, LabelLength,
                LabelHeight);
            _dropdown.Dimension =
                new Rectangle(new Point(SettingsX,
                    SettingsBaseY + LabelHeight + LabelsPosX + (LabelHeight - GlobalVariables.DropdownSize.Y)), GlobalVariables.DropdownSize);
            _backB.Dimension = new Rectangle(_posBb, GlobalVariables.MenuButtonsSize);
            _applyB.Dimension = new Rectangle(_posAb, GlobalVariables.MenuButtonsSize);
        }
    }
}
