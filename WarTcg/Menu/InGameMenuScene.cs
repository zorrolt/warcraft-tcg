﻿using System;
using EngineWar;
using EngineWar.GUI;
using Microsoft.Xna.Framework;

namespace WarTcg.Menu
{
    internal class InGameMenuScene : Scene
    {
        public const string Resume = "Resume",
            ToMainBtLabel = "To Main Menu",
            ExitBtLabel = "Exit";
        private const int topButtonY = 100,
            buttonSpace = 200;

        private Button _resume, _toMenu, _exit;

        public InGameMenuScene(): base(GlobalVariables.InGameMenuSceneName)
        {
        }

        public override void Initialize()
        {
            var mid = (ScreenManager.Instance.Engine.GetResolution().X - GlobalVariables.MenuButtonsSize.X) / 2;
            _resume =
                new Button(new Rectangle(new Point(mid, topButtonY), GlobalVariables.MenuButtonsSize),
                        () => ScreenManager.Instance.SetScreen(GlobalVariables.GameScreenName),
                        GlobalVariables.ButtonContentName, GlobalVariables.MenuFontName)
                { Text = Resume };
            _toMenu =
                new Button(new Rectangle(new Point(mid, topButtonY + buttonSpace), GlobalVariables.MenuButtonsSize),
                        () =>
                        {
                            MessageHandler.SendMessage(GlobalVariables.ToMainMenu);
                            ScreenManager.Instance.UnLoadScreen(GlobalVariables.GameScreenName);
                        }, GlobalVariables.ButtonContentName, GlobalVariables.MenuFontName)
                { Text = ToMainBtLabel };
            _exit =
                new Button(new Rectangle(new Point(mid, topButtonY + buttonSpace*2), GlobalVariables.MenuButtonsSize),
                        () => ScreenManager.Instance.Engine.Close(), GlobalVariables.ButtonContentName,
                        GlobalVariables.MenuFontName)
                { Text = ExitBtLabel };
            EngineWar.MessageHandler.Instance.SubscribeMessage(EngineWar.GlobalVariables.ResolutionChangedMessage, (sender, args) => RearrangeUi());
            AddElement(Scene.Layers.Top, _resume);
            AddElement(Scene.Layers.Top, _toMenu);
            AddElement(Scene.Layers.Top, _exit);
        }

        private void RearrangeUi()
        {
            var mid = (ScreenManager.Instance.Engine.GetResolution().X - GlobalVariables.MenuButtonsSize.X) / 2;
            _resume.Dimension = new Rectangle(new Point(mid, topButtonY), GlobalVariables.MenuButtonsSize);
            _toMenu.Dimension = new Rectangle(new Point(mid, topButtonY + buttonSpace), GlobalVariables.MenuButtonsSize);
            _exit.Dimension = new Rectangle(new Point(mid, topButtonY + buttonSpace * 2), GlobalVariables.MenuButtonsSize);
        }
    }
}
