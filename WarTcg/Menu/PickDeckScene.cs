﻿using System.Collections.Generic;
using EngineWar;
using EngineWar.GUI;
using Microsoft.Xna.Framework;
using WarTcg.DataEntities.DeckData;

namespace WarTcg.Menu
{
    //TODO: deck select screen (more than 2 decks)
    class PickDeckScene : Scene
    {
        private const string PickText = "Pick your side";
        private readonly Button _hordeDeckBt, _allyDeckBt;
        private Picture _background;
        private TextBox _pickLabel;

        public PickDeckScene(List<DeckData> decks) : base(GlobalVariables.PickDeckSceneName)
        {
            _hordeDeckBt = new Button(new Rectangle(1380, 200, 300, 300), () =>
            {
                MessageHandler.SendMessage(GlobalVariables.StartGame, this,
                    new MenuScreen.StartGameArgs {EnemyDeck = decks[1], PlayerDeck = decks[0]});
            }, GlobalVariables.HordeButtonContentName);

            _allyDeckBt = new Button(new Rectangle(300, 200, 300, 300), () =>
                {
                    MessageHandler.SendMessage(GlobalVariables.StartGame, this,
                        new MenuScreen.StartGameArgs {EnemyDeck = decks[0], PlayerDeck = decks[1]});
                }, GlobalVariables.AllianceButtonContentName
            );
        }

        public override void Initialize()
        {
            //Hardcoded decks
            EngineWar.MessageHandler.Instance.SubscribeMessage(EngineWar.GlobalVariables.ResolutionChangedMessage, (sender, args) => RearrangeUi());
            _background = new Picture(GlobalVariables.PickDeckBackground);
            var labelWidth = 400;
            var res = ScreenManager.Instance.Engine.GetResolution();
            _pickLabel =
                new TextBox(
                    new Rectangle(res.X / 2 - labelWidth / 2, 600, labelWidth,
                        200), PickText)
                {
                    FontName = GlobalVariables.MenuFontName,
                    FontColor = Color.White,
                    FontSize = 1.4f
                };
            var buttonsY = 200;
            var buttonWidth = 300;
            var buttonSpace = (res.X / 2 - buttonWidth) / 2;
            _hordeDeckBt.Dimension = new Rectangle(res.X - (buttonSpace + buttonWidth) , buttonsY, buttonWidth, 300);
            _allyDeckBt.Dimension = new Rectangle(buttonSpace, buttonsY, buttonWidth, 300);
            _pickLabel.SetBackgroundColor(Color.Transparent);
            AddElement(Layers.Ground, _background);
            AddElement(Layers.Top, _pickLabel);
            AddElement(Layers.Top, _hordeDeckBt);
            AddElement(Layers.Top, _allyDeckBt);
        }

        private void RearrangeUi()
        {
            var labelWidth = 400;
            var res = ScreenManager.Instance.Engine.GetResolution();
            _pickLabel.Dimension =
                new Rectangle(res.X / 2 - labelWidth / 2, 600, labelWidth,
                    200);
            var buttonsY = 200;
            var buttonWidth = 300;
            var buttonSpace = (res.X / 2 - buttonWidth) / 2;
            _hordeDeckBt.Dimension = new Rectangle(res.X - (buttonSpace + buttonWidth), buttonsY, buttonWidth, 300);
            _allyDeckBt.Dimension = new Rectangle(buttonSpace, buttonsY, buttonWidth, 300);
            _background.Dimension = new Rectangle(new Point(), res);
        }
    }
}
