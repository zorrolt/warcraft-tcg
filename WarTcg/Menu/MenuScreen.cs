﻿using System;
using System.Collections.Generic;
using EngineWar;
using EngineWar.GUI;
using Microsoft.Xna.Framework;
using WarTcg.DataEntities.DeckData;
using WarTcg.Game;

namespace WarTcg.Menu
{
    internal class MenuScreen : Screen
    {
        private readonly LoadingScene _loadingScene;
        private TextBox _wonLabel;
        private Picture _wonBg, _lostBg;

        public class StartGameArgs : EventArgs
        {
            public DeckData EnemyDeck, PlayerDeck;
        }

        public MenuScreen() : base(GlobalVariables.MenuScreenName, GlobalVariables.MenuContentFile)
        {
            // Register messages
            MessageHandler.RegisterMessage(GlobalVariables.ToMainMenu);
            MessageHandler.RegisterMessage(GlobalVariables.GameEnded);
            MessageHandler.RegisterMessage(GlobalVariables.ToSettingsMenu);
            MessageHandler.RegisterMessage(GlobalVariables.NewGame);
            MessageHandler.RegisterMessage(GlobalVariables.StartGame);

            MessageHandler.SubscribeMessage(GlobalVariables.GameEnded,
                (sender, args) => OnGameEnd(args as GameManager.PlayerArgs));
            MessageHandler.SubscribeMessage(GlobalVariables.ToMainMenu,
                (sender, args) => SetScene(GlobalVariables.MainMenuSceneName));
            MessageHandler.SubscribeMessage(GlobalVariables.ToSettingsMenu,
                (sender, args) => SetScene(GlobalVariables.SettingsSceneName));
            MessageHandler.SubscribeMessage(GlobalVariables.NewGame,
                (sender, args) => SetScene(GlobalVariables.PickDeckSceneName));
            MessageHandler.SubscribeMessage(GlobalVariables.StartGame,
                (sender, args) => LoadGame(args as StartGameArgs));

            var dataReader = new DataReader(GlobalVariables.DecksFile);
            var decks = dataReader.Get<List<DeckData>>();
            dataReader.Close();

            _loadingScene = new LoadingScene();

            var deckScene = new PickDeckScene(decks);
            var mainMenu = new MainMenu();
            var inGameMenu = new InGameMenuScene();

            var settingsMenu = new SettingsMenu();

            
            
            AddScene(_loadingScene);
            AddScene(deckScene);
            AddScene(mainMenu);
            AddScene(inGameMenu);
            AddScene(settingsMenu);
            SetScene(GlobalVariables.MainMenuSceneName);
        }

        public override void OnLoad()
        {
            
        }

        public override void OnUnload()
        {
            
        }

        public override void Initialize()
        {
            var gameOverScene = new EndGameScene();
            _wonBg = new Picture(GlobalVariables.WonBackground) { Show = false };
            _lostBg = new Picture(GlobalVariables.LostBackground) { Show = false };
            gameOverScene.AddElement(Scene.Layers.Ground, _wonBg);
            gameOverScene.AddElement(Scene.Layers.Ground, _lostBg);
            gameOverScene.AddElement(Scene.Layers.Top,
                new Button(
                    new Rectangle(
                        new Point(
                            ScreenManager.Instance.Engine.GetResolution().X / 2 - GlobalVariables.MenuButtonsSize.X / 2,
                            (int)(ScreenManager.Instance.Engine.GetResolution().Y * 0.8)),
                        GlobalVariables.MenuButtonsSize),
                    "To Menu",
                    () =>
                    {
                        _wonBg.Show = false;
                        _lostBg.Show = true;
                        SetScene(GlobalVariables.MainMenuSceneName);
                    }, GlobalVariables.ButtonContentName,
                    GlobalVariables.MenuFontName));
            _wonLabel = new TextBox(new Rectangle(
                new Point(
                    ScreenManager.Instance.Engine.GetResolution().X / 2 - GlobalVariables.MenuButtonsSize.X / 2,
                    (int)(ScreenManager.Instance.Engine.GetResolution().Y * 0.2)),
                GlobalVariables.MenuButtonsSize * new Point(2)))
            { FontColor = Color.White, FontName = GlobalVariables.MenuFontName };
            _wonLabel.SetBackgroundColor(Color.Transparent);
            gameOverScene.AddElement(Scene.Layers.Top, _wonLabel);
            AddScene(gameOverScene);
        }

        public void OnGameEnd(GameManager.PlayerArgs data)
        {
            SetScene(GlobalVariables.GameOverScene);
            if (data.Player == GlobalVariables.Turn.Player)
            {
                _wonBg.Show = true;
                _wonLabel.Text = "You won!";
            }
            else
            {
                _lostBg.Show = true;
                _wonLabel.Text = "You lost!";
            }
            SetScene(GlobalVariables.GameOverScene);
            ScreenManager.Instance.SetScreen(GlobalVariables.MenuScreenName);
            ScreenManager.Instance.UnLoadScreen(GlobalVariables.GameScreenName);
        }

        private void LoadGame(StartGameArgs decksData)
        {
            SetScene(GlobalVariables.LoadingSceneName);
            _loadingScene.LoadGame(decksData.PlayerDeck, decksData.EnemyDeck);
        }

        public void ToMenu()
        {
            SetScene(GlobalVariables.InGameMenuSceneName);
        }
    }
}