﻿using System;
using EngineWar;
using EngineWar.GUI;
using Microsoft.Xna.Framework;

namespace WarTcg.Menu
{
    internal class MainMenu : Scene
    {
        public const string NewGameBtLabel = "Demo", 
            ExitBtLabel = "Exit", SettingsLabel = "Settings";

        private Button _newGame, _settings, _exit;

        public MainMenu() : base(GlobalVariables.MainMenuSceneName)
        {
        }

        public override void Initialize()
        {
            var screenSide = ScreenManager.Instance.Engine.GetResolution() / new Point(2, 1);
            var buttonsX = (screenSide.X - GlobalVariables.MenuButtonsSize.X) / 2;
            int buttonY = 200;
            _newGame =
                new Button(new Rectangle(new Point(buttonsX, buttonY), GlobalVariables.MenuButtonsSize),
                    () => MessageHandler.SendMessage(GlobalVariables.NewGame),
                    GlobalVariables.ButtonContentName, GlobalVariables.MenuFontName)
                { Text = NewGameBtLabel };
            _settings =
                new Button(new Rectangle(new Point(buttonsX, (int) (buttonY + GlobalVariables.MenuButtonsSize.Y*1.5f)), GlobalVariables.MenuButtonsSize),
                    () => MessageHandler.SendMessage(GlobalVariables.ToSettingsMenu),
                    GlobalVariables.ButtonContentName, GlobalVariables.MenuFontName)
                { Text = SettingsLabel };
            _exit =
                new Button(new Rectangle(new Point(buttonsX, buttonY + GlobalVariables.MenuButtonsSize.Y * 3), GlobalVariables.MenuButtonsSize),
                        () => ScreenManager.Instance.Engine.Close(), GlobalVariables.ButtonContentName,
                        GlobalVariables.MenuFontName)
                { Text = ExitBtLabel };
            EngineWar.MessageHandler.Instance.SubscribeMessage(EngineWar.GlobalVariables.ResolutionChangedMessage, (sender, args) => RearrangeUi());
            AddElement(Layers.Top, _newGame);
            AddElement(Layers.Top, _settings);
            AddElement(Layers.Top, _exit);
        }

        private void RearrangeUi()
        {
            var screenSide = ScreenManager.Instance.Engine.GetResolution() / new Point(2, 1);
            var buttonsX = (screenSide.X - GlobalVariables.MenuButtonsSize.X) / 2;
            int buttonY = 200;
            _newGame.Dimension =
                new Rectangle(new Point(buttonsX, buttonY), GlobalVariables.MenuButtonsSize);
            _settings.Dimension =
                new Rectangle(new Point(buttonsX, (int) (buttonY + GlobalVariables.MenuButtonsSize.Y * 1.5f)),
                    GlobalVariables.MenuButtonsSize);
            _exit.Dimension =
                new Rectangle(new Point(buttonsX, buttonY + GlobalVariables.MenuButtonsSize.Y * 3),
                    GlobalVariables.MenuButtonsSize);
        }
    }
}
