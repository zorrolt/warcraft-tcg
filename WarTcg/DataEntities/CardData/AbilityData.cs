﻿namespace WarTcg.DataEntities.CardData
{
    public sealed class AbilityData : CardBaseData
    {
        public enum AbilitySubType
        {
            Shout
        }

        public enum AbilityType
        {
            Normal,
            Instant
        }

        public ClassType Class;
        public SubClassType SubClass;
        public AbilitySubType SubType;
        public AbilityType Type;
    }
}