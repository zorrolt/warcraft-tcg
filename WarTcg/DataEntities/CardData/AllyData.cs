﻿namespace WarTcg.DataEntities.CardData
{
    public sealed class AllyData : UnitCardData
    {
        public int Atk;
        public AttackType AtkType;
    }
}