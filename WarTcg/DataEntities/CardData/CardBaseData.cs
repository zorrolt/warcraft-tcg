﻿using System.Collections.Generic;
using WarTcg.Game.Cards;

namespace WarTcg.DataEntities.CardData
{
    public class CardBaseData
    {
        public class CardEffectData
        {
            public enum EffectType { Attach, DoDamage, Target, Active, Destroy, Buff }
            public enum EffectTargetType { None, Hero, Ally, Unit, Attached, EnemyUnit, Weapon, Targeted, DamagedAlly, PlayerAlly, EnemyAlly }
            public enum ValueBonusType { None, TargetedWeapon }
            public enum BuffType { None, Atk }

            public bool Ongoing = false;
            public bool TapTarget = false;
            public EffectType Type = EffectType.Active;
            public EffectTargetType TargetType = EffectTargetType.None;
            public int EffectValue = 0;
            public int TargetsCount = 0;
            public ValueBonusType ValueBonus = ValueBonusType.None;
            public BuffType BuffTarget = BuffType.None;

            public bool CheckTargetType(Card card)
            {
                return (card.CardType == Card.CardTypes.Ally ||
                       card.CardType == Card.CardTypes.Hero) && TargetType == EffectTargetType.Unit;
            }
        }

        public enum AttackType
        {
            Mele,
            Range,
            Fire,
            Shadow,
            Arcane,
            Frost,
            Holy
        }

        public enum ClassType
        {
            Mage,
            Warrior,
            Paladin,
            Shaman,
            Hunter,
            Priest,
            Rogue,
            Druid,
            Warlock
        }

        public enum SubClassType
        {
            Fire,
            Frost,
            Arane,
            Protection,
            Fury,
            Arms
        }

        public string ContentName;
        public int Cost = 0;
        public List<CardEffectData> Effect = new List<CardEffectData>();

        public string Name;
        public string Text;
    }
}