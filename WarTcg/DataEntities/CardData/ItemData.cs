﻿namespace WarTcg.DataEntities.CardData
{
    public abstract class ItemData : CardBaseData
    {
        public enum SlotType
        {
            Head,
            MainHand,
            Hands
        }

        public int Durability;

        public ClassType[] Classes;

        public SlotType Slot;
    }
}