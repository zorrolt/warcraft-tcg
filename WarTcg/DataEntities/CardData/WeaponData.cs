﻿namespace WarTcg.DataEntities.CardData
{
    public sealed class WeaponData : ItemData
    {
        public enum WeaponType
        {
            Axe,
            Sword
        }

        public int Atk;
        public AttackType AtkType;

        public WeaponType Type;
    }
}