﻿namespace WarTcg.DataEntities.CardData
{
    public sealed class ArmorData : ItemData
    {
        public enum ArmorType
        {
            Cloth,
            Plate,
            Mail
        }

        public ArmorType Type;
    }
}