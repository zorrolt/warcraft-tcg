﻿namespace WarTcg.DataEntities.CardData
{
    public abstract class UnitCardData : CardBaseData
    {
        public enum FactionType
        {
            Ally,
            Horde
        }

        public enum RaceType
        {
            Troll,
            Dwarf,
            Tauren,
            Orc,
            Undead,
            Gnome,
            Human,
            NightElf,
            BloodElf
        }

        public ClassType Class;
        public FactionType Faction;
        public int Health;

        public RaceType Race;
    }
}