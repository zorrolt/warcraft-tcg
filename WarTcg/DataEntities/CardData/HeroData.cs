﻿namespace WarTcg.DataEntities.CardData
{
    public sealed class HeroData : UnitCardData
    {
        public enum Profession
        {
            Tailoring,
            Enchanting,
            Blacksmithing,
            Mining
        }

        public string BackContentName;
        public Profession Prof1;
        public Profession Prof2;

        public SubClassType SubClass;
    }
}