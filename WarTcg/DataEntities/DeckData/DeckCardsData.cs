﻿using System.Collections.Generic;

namespace WarTcg.DataEntities.DeckData
{
    public class DeckCardsData
    {
        public List<DeckCardData> Allies { get; set; }
        public List<DeckCardData> Quests { get; set; }
        public List<DeckCardData> Abilities { get; set; }
        public ItemsDeckData Items { get; set; }

        public class ItemsDeckData
        {
            public List<DeckCardData> Armors { get; set; }
            public List<DeckCardData> Weapons { get; set; }
        }
    }
}