﻿using WarTcg.DataEntities.CardData;

namespace WarTcg.DataEntities.DeckData
{
    public class DeckData
    {
        public UnitCardData.FactionType Faction;
        public string File;
        public string Hero;
        public string Name;
        public int TotalCards;
    }
}