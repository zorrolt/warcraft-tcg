﻿using WarTcg.DataEntities.CardData;

namespace WarTcg.DataEntities
{
    public sealed class CardsData
    {
        public AbilityData[] Abilities;
        public AllyData[] Allies;

        public HeroData[] Heroes;
        public ItemCards Items;
        public CardBaseData[] Quests;

        public class ItemCards
        {
            public ArmorData[] Armors;
            public WeaponData[] Weapons;
        }
    }
}