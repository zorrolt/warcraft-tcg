﻿using System.Collections.Generic;
using WarTcg.Game.Cards;

namespace WarTcg.Game
{
    public class Hand
    {
        public List<Card> Cards;

        public Hand()
        {
            Cards = new List<Card>();
        }

        public void Put(Card card)
        {
            Cards.Add(card);
        }
    }
}