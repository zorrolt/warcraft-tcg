﻿using System;
using System.Collections.Generic;
using WarTcg.Game.Cards;

namespace WarTcg.Game
{
    public class Ai
    {

        public Ai()
        {
        }

        public int Discard(List<Card> hand)
        {
            int index = 0, max = 0;
            for(int i = 1; i < hand.Count - GlobalVariables.HandCardsCount; i++)
                if (hand[i].Cost < max)
                {
                    index = i;
                    max = hand[i].Cost;
                }
            return index;
        }

        public bool Mulligan(List<Card> hand)
        {
            return hand.FindAll(c => c.Cost == 1).Count < 1 || hand.FindAll(c => c.Cost == 2).Count < 2;
        }

        public void Play(Board aiBoard, Board playerBoard)
        {
            bool resourceSet = false;
            Card questCard = aiBoard.Hand.Cards.Find(c => c is QuestCard);
            if (questCard != null)
            {
                MessageHandler.SendMessage(GlobalVariables.CardClicked, this, new Card.CardEventArgs{Card = questCard});
                resourceSet = true;
            }
            int lowestCost = Int32.MaxValue;
            Card cheapestAlly = null;
            foreach (var card in aiBoard.Hand.Cards)
            {
                if (card is AllyCard && lowestCost > card.Cost)
                {
                    cheapestAlly = card;
                    lowestCost = cheapestAlly.Cost;
                }
            }
            if (cheapestAlly != null)
            {
                if (aiBoard.Resources.Count >= lowestCost)
                    MessageHandler.SendMessage(GlobalVariables.CardClicked, this, new Card.CardEventArgs{ Card = cheapestAlly });
                else if (!resourceSet)
                {
                    int maxCost = -1;
                    Card expensiveCard = null;
                    foreach (var card in aiBoard.Hand.Cards)
                    {
                        if (maxCost < card.Cost)
                            expensiveCard = card;
                    }
                    MessageHandler.SendMessage(GlobalVariables.CardRightClicked, this, new Card.CardEventArgs { Card = expensiveCard});
                    if (aiBoard.Resources.Count >= lowestCost && expensiveCard != cheapestAlly)
                        MessageHandler.SendMessage(GlobalVariables.CardClicked, this, new Card.CardEventArgs { Card = cheapestAlly});
                }
            }
            List<Tuple<UnitCard, UnitCard>> _battles = new List<Tuple<UnitCard, UnitCard>>();
            for (var i = 0; i < aiBoard.Allies.Count; i++)
            {
                UnitCard ally = aiBoard.Allies[i] as AllyCard;
                if (ally.IsTapped)
                    continue;
                AllyCard attackAlly = (AllyCard) ally;
                AllyCard pAllyToAttack = null;
                foreach (var card in playerBoard.Allies)
                {
                    var pAlly = (AllyCard) card;
                    if (attackAlly?.Attack >= pAlly.Health)
                    {
                        if (pAlly.Attack < attackAlly?.Health)
                        {
                            if (pAllyToAttack == null ||
                                pAllyToAttack.Attack >= pAlly.Attack && pAllyToAttack.Health >= pAlly.Attack)
                                pAllyToAttack = pAlly;
                        }
                        else if (pAlly.Attack > attackAlly?.Attack)
                        {
                            if (pAllyToAttack == null ||
                                pAllyToAttack.Attack >= pAlly.Attack && pAllyToAttack.Health >= pAlly.Attack)
                                pAllyToAttack = pAlly;
                        }
                    }
                }
                MessageHandler.SendMessage(GlobalVariables.CardClicked, this, new Card.CardEventArgs { Card = ally});
                MessageHandler.SendMessage(GlobalVariables.CardClicked, this,
                   new Card.CardEventArgs { Card = pAllyToAttack ?? playerBoard.Hero as UnitCard});
                if (ally.IsDead)
                    i--;
            }

            if (aiBoard.Hand.Cards.Count > GlobalVariables.HandCardsCount && !resourceSet)
            {
                lowestCost = Int32.MaxValue;
                Card cheapestCard = null;
                foreach (var card in aiBoard.Hand.Cards)
                {
                    if (lowestCost > card.Cost)
                    {
                        cheapestCard = card;
                        lowestCost = cheapestCard.Cost;
                    }
                }
                if(cheapestCard != null)
                        MessageHandler.SendMessage(GlobalVariables.CardClicked, this, new Card.CardEventArgs { Card = cheapestCard});
            }

            MessageHandler.SendMessage(GlobalVariables.EndTurn);
        }

    }
}