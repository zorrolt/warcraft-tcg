﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EngineWar;
using EngineWar.GUI;
using Microsoft.Xna.Framework;
using WarTcg.Game.Cards;

namespace WarTcg.Game
{
    class MainScene : Scene
    {

        private Picture _deck1, _deck2;
        private Dialog _statrtDialog, _popup;
        private Button _endTurnButton, _btnMlgY, _btnMlgN;
        private readonly Picture _highlight;

        public class EnableEndTurnArgs : EventArgs
        {
            public bool Enable;
        }

        private const int TurnPopupH = 200,
            TurnPopupW = 400;

        private const string EndTurnButtonTxt = "End Turn";

        private const int StartDialWidth = 500,
            StartDialHeight = 300,
            StartDialY = 100,
            DialogBtOffset = 100,
            BtWidth = 100,
            BtHeight = 50;

        public MainScene() : base(GlobalVariables.MainGameScene)
        {
            MessageHandler.SubscribeMessage(GlobalVariables.CardHover,
    (sender, args) => OnCardHover(sender as Card));
            MessageHandler.SubscribeMessage(GlobalVariables.CardLeave, (sender, args) => OnCardLeave());
            _highlight = new Picture(GlobalVariables.DefaultCardHighlight) { Show = false };
            MessageHandler.SubscribeMessage(GlobalVariables.EnableEndTurn, (sender, args) =>
            {
                _endTurnButton.IsEnabled = (args as EnableEndTurnArgs).Enable;
            });
            MessageHandler.SubscribeMessage(GlobalVariables.Discard, (sender, args) =>
            {
                if ((args as GameManager.PlayerArgs).Player == GlobalVariables.Turn.Player)
                {
                    _popup.Show = true;
                    _popup.Text = "Discard a card";
                }
            });
            MessageHandler.SubscribeMessage(GlobalVariables.Discarded, (sender, args) => _popup.Show = false);
            MessageHandler.SubscribeMessage(GlobalVariables.ShowMessage, (sender, args) =>
            {
                _popup.Show = true;
                _popup.Text = (args as GameScreen.MessageArgs)?.Text;
            });
            MessageHandler.SubscribeMessage(GlobalVariables.HideMessage, (sender, args) => _popup.Show = false);
            MessageHandler.SubscribeMessage(GlobalVariables.ActionStarted, (sender, args) => OnTurnStarted((args as GameManager.PlayerArgs).Player));
            MessageHandler.SubscribeMessage(GlobalVariables.TurnStarted, (sender, args) => ShowTurnTitle((args as GameManager.PlayerArgs).Player));
        }

        public override void Initialize()
        {
            _popup = new Dialog("", "",
                new Rectangle(ScreenManager.Instance.Engine.GetResolution().X / 2 - TurnPopupW / 2,
                    ScreenManager.Instance.Engine.GetResolution().Y / 2 - TurnPopupH / 2, TurnPopupW, TurnPopupH),
                Color.White, new List<Button>(), font: GlobalVariables.MenuFontName)
            {
                ShowExitButton = false,
                Show = false
            };

            _endTurnButton =
                new Button(
                        new Rectangle(
                            new Point(GlobalVariables.SpacesCard,
                                ScreenManager.Instance.Engine.GetResolution().Y - GlobalVariables.BoardCardDim.Y * 3 -
                                GlobalVariables.EndTurnButtonDim.Y), GlobalVariables.EndTurnButtonDim),
                        EndTurnButtonTxt, () =>
                        {
                            _endTurnButton.IsEnabled = false;
                            MessageHandler.SendMessage(GlobalVariables.EndTurn);
                        },
                        GlobalVariables.ButtonContentName, GlobalVariables.BoardFontName)
                    {IsEnabled = false};

            var dialogDim = new Rectangle(ScreenManager.Instance.Engine.GetResolution().X / 2 - StartDialWidth / 2,
                StartDialY,
                StartDialWidth, StartDialHeight);

            _btnMlgY =
                new Button(
                    new Rectangle(dialogDim.X + DialogBtOffset, dialogDim.Y + dialogDim.Height - DialogBtOffset, BtWidth,
                        BtHeight), "Yes", () =>
                    {
                        MessageHandler.SendMessage(GlobalVariables.MulliganMessage, this,
                            new GameScreen.MulliganArgs {Mulligan = true, Player = GlobalVariables.Turn.Player});
                        _statrtDialog.Show = false;
                    }, GlobalVariables.ButtonContentName, GlobalVariables.BoardFontName)
                {
                    IsAutoLabelSize = true
                };

            _btnMlgN =
                new Button(
                    new Rectangle(dialogDim.X + dialogDim.Width - (BtWidth + DialogBtOffset),
                        dialogDim.Y + dialogDim.Height - DialogBtOffset, BtWidth,
                        BtHeight), "No", () =>
                    {
                        MessageHandler.SendMessage(GlobalVariables.MulliganMessage, this,
                            new GameScreen.MulliganArgs {Mulligan = false, Player = GlobalVariables.Turn.Player});
                        _statrtDialog.Show = false;
                    }, GlobalVariables.ButtonContentName, GlobalVariables.BoardFontName)
                {
                    IsAutoLabelSize = true
                };
            _statrtDialog = new Dialog("You are starting ", "", dialogDim
                    , Color.White, new List<Button> {_btnMlgY, _btnMlgN}, font: GlobalVariables.BoardFontName)
                {ShowExitButton = false};

            EngineWar.MessageHandler.Instance.SubscribeMessage(EngineWar.GlobalVariables.ResolutionChangedMessage,
                (sender, args) => RearrangeUi());

            _deck1 =
                new Picture(
                    new Rectangle(
                        new Point(
                            GlobalVariables.SpacesCard,
                            GlobalVariables.P1ResourceZoneDim.Y),
                        GlobalVariables.BoardCardDim), GlobalVariables.DefaultCardsBack);
            _deck2 =
                new Picture(
                    new Rectangle(
                        new Point(
                            ScreenManager.Instance.Engine.GetResolution().X - GlobalVariables.SpacesCard - GlobalVariables.BoardCardDim.X,
                            GlobalVariables.P2ResourceZoneDim.Y),
                        GlobalVariables.BoardCardDim),
                    GlobalVariables.DefaultCardsBack);
        }

        private void RearrangeUi()
        {
            var dialogDim = new Rectangle(ScreenManager.Instance.Engine.GetResolution().X / 2 - StartDialWidth / 2,
                StartDialY,
                StartDialWidth, StartDialHeight);
            _popup.Dimension = new Rectangle(ScreenManager.Instance.Engine.GetResolution().X / 2 - TurnPopupW / 2,
                ScreenManager.Instance.Engine.GetResolution().Y / 2 - TurnPopupH / 2, TurnPopupW, TurnPopupH);
            _endTurnButton.Dimension =
                new Rectangle(
                    new Point(GlobalVariables.SpacesCard,
                        ScreenManager.Instance.Engine.GetResolution().Y - GlobalVariables.BoardCardDim.Y * 3 -
                        GlobalVariables.EndTurnButtonDim.Y), GlobalVariables.EndTurnButtonDim);
            _statrtDialog.Dimension = dialogDim;
            _btnMlgN.Dimension = new Rectangle(dialogDim.X + dialogDim.Width - (BtWidth + DialogBtOffset),
                dialogDim.Y + dialogDim.Height - DialogBtOffset, BtWidth,
                BtHeight);
            _btnMlgY.Dimension = new Rectangle(dialogDim.X + DialogBtOffset, dialogDim.Y + dialogDim.Height - DialogBtOffset,
                BtWidth,
                BtHeight);
            _deck1.Dimension = new Rectangle(
                        new Point(
                            GlobalVariables.SpacesCard,
                            GlobalVariables.P1ResourceZoneDim.Y),
                        GlobalVariables.BoardCardDim);
            _deck2.Dimension = new Rectangle(
                        new Point(
                            ScreenManager.Instance.Engine.GetResolution().X - GlobalVariables.SpacesCard - GlobalVariables.BoardCardDim.X,
                            GlobalVariables.P2ResourceZoneDim.Y),
                        GlobalVariables.BoardCardDim);
        }

        private void ShowTurnTitle(GlobalVariables.Turn turn)
        {
            _popup.Text = turn == GlobalVariables.Turn.Player2 ? "Player2 turn !" : "Your turn !";
            _popup.Show = true;
            Task.Delay(1000).ContinueWith(t =>
            {
                _popup.Show = false;
                MessageHandler.SendMessage(GlobalVariables.NextTurn);
            });
        }

        public void OnLoad()
        {
            AddElement(Layers.Top, _popup);
            AddElement(Layers.Top, _statrtDialog);
            AddElement(Layers.Top, _endTurnButton);
            AddElement(Layers.Third, _deck1);
            AddElement(Layers.Third, _deck2);
            AddElement(Layers.First, _highlight);
        }

        public void SetDialog(GlobalVariables.Turn turn)
        {
            _statrtDialog.Show = true;
            _statrtDialog.Text = "You are starting ";
            _statrtDialog.Text += (turn == GlobalVariables.Turn.Player ? "first" : "second") + ". Do mulligan ?";
        }

        private void OnTurnStarted(GlobalVariables.Turn turn)
        {
            if (turn == GlobalVariables.Turn.Player)
                _endTurnButton.IsEnabled = true;
        }

        private void OnCardHover(Card card)
        {
            if (!card.Facedown && card.Show)
            {
                if (card.IsZoomed)
                    return;
                _highlight.Dimension = new Rectangle(card.Dimension.Location - new Point(10),
                    card.Dimension.Size + new Point(20));
                _highlight.Show = true;
            }
        }

        private void OnCardLeave()
        {
            _highlight.Show = false;
        }

    }
}
