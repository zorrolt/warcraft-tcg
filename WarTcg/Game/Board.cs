﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using EngineWar;
using Microsoft.Xna.Framework;
using WarTcg.Game.Cards;

namespace WarTcg.Game
{
    public class Board
    {
        public List<Card> Graveyard;
        public List<Card> Allies;
        public HeroCard Hero;
        public List<Card> Resources;
        public List<Card> Removed;
        public List<Card> HeroLine; 
        public Deck Deck;
        public Hand Hand;
        private readonly bool _isHost;

        private List<Card> _cards;

        public void Untap()
        {
            Hero.SetTapped(false);
            foreach (var resource in Resources)
            {
                resource.SetTapped(false);
            }
            foreach (var ally in Allies)
            {
                ally.SetTapped(false);
            }
            RearrangeZone(Resources,
                _isHost
                    ? GlobalVariables.P1ResourceZoneDim
                    : GlobalVariables.P2ResourceZoneDim );
            RearrangeZone(Allies, _isHost ? GlobalVariables.P1AllyZoneDim : GlobalVariables.P2AllyZoneDim);
        }


        public void RearrangeAlliesZone()
        {
            RearrangeZone(Allies, _isHost ? GlobalVariables.P1AllyZoneDim : GlobalVariables.P2AllyZoneDim);
        }

        public void RearrangeHeroLineZone()
        {
            RearrangeZone(HeroLine, _isHost ? GlobalVariables.P1HeroZoneDim : GlobalVariables.P2HeroZoneDim);
        }

        public Board(List<Card> cards, bool isHost, HeroCard hero)
        {
            Hero = hero;
            Hero.Show = true;
            Hero.Dimension = isHost
                ? new Rectangle(new Point(GlobalVariables.SpacesCard, GlobalVariables.P1HeroZoneDim.Y),
                    GlobalVariables.BoardCardDim)
                : new Rectangle(new Point(ScreenManager.Instance.Engine.GetResolution().X - GlobalVariables.BoardCardDim.X -
                            GlobalVariables.SpacesCard, GlobalVariables.P2HeroZoneDim.Y),
                    GlobalVariables.BoardCardDim);
            _isHost = isHost;
            _cards = cards;
            Graveyard = new List<Card>();
            Allies = new List<Card>();
            Resources = new List<Card>();
            Removed = new List<Card>();
            HeroLine = new List<Card>();
            Hand = new Hand();
            Deck = new Deck {Cards = new Stack<Card>(_cards)};
            MessageHandler.SubscribeMessage(GlobalVariables.CardDied, (sender, args) => OnCardDied(sender as AllyCard));
        }

        public void FirstDraw()
        {
            for (var index = 0; index < GlobalVariables.HandCardsCount; index++)
                Draw();
        }

        public void Draw()
        {
            var id = Deck.Draw().ToString();
            var index = _cards.FindIndex(c => c.Id.ToString() == id);
            var card = _cards[index];
            if(_isHost)
                card.TurnUp();
            else
                card.TurnDown();
            card.Show = true;
            Hand.Put(card);
            card.ToHand();
            RearrangeHand();
        }

        public void RemoveHeroLineCard(Card card)
        {
            if (HeroLine.Contains(card))
            {
                HeroLine.Remove(card);
                PutToGraveyard(card);
            }
        }

        private void OnCardDied(AllyCard card)
        {
            for (int i = 0; i < HeroLine.Count; i++)
            {
                var abilityCard = HeroLine[i] as AbilityCard;
                if (abilityCard != null && abilityCard.AttachedTo == card)
                {
                    HeroLine.RemoveAt(i);
                    PutToGraveyard(abilityCard);
                    i--;
                }
            }
            if (Allies.Contains(card))
            {
                card.AttachedTo = null;
                card.SetTapped(false);
                Debug.WriteLine("Card died: " + card.Name);
                Allies.Remove(card);
                PutToGraveyard(card);
            }
        }

        private void Pay(int cost)
        {
            int res = 0;
            if (cost == 0)
                return;
            foreach (var t in Resources)
            {
                if (!t.IsTapped)
                {
                    res++;
                    t.SetTapped(true);
                }
                if (res == cost)
                    return;
            }
        }

        public bool PutAsResource(Card card)
        {
            if (Hand.Cards.Contains(card))
            {
                card.Dimension = new Rectangle(new Point(), GlobalVariables.BoardCardDim);
                if (card.CardType != Card.CardTypes.Quest)
                    card.TurnDown();
                Hand.Cards.Remove(card);
                RearrangeHand();
                PutResource(card);
                card.ToBoard();
                return true;
            }
            return false;
        }

        public void PutResource(Card card)
        {
            Resources.Add(card);
            RearrangeZone(Resources,
                _isHost ? GlobalVariables.P1ResourceZoneDim : GlobalVariables.P2ResourceZoneDim);
        }

        public bool Put(Card card)
        {
            if (Hand.Cards.Contains(card) && card.Cost <= Resources.FindAll(c => !c.IsTapped).Count)
            {
                Pay(card.Cost);
                Hand.Cards.Remove(card);
                RearrangeHand();
                card.Show = true;
                card.TurnUp();
                card.Dimension = new Rectangle(new Point(), GlobalVariables.BoardCardDim);
                switch (card.CardType)
                {
                    case Card.CardTypes.Quest:
                        PutResource(card);
                        break;
                    case Card.CardTypes.Ally:
                        Allies.Add(card);
                        var unitCard = card as UnitCard;
                        if (unitCard != null) unitCard.HealthIco.Show = true;
                        card.SetTapped(true);
                        RearrangeZone(Allies,
                            _isHost ? GlobalVariables.P1AllyZoneDim : GlobalVariables.P2AllyZoneDim);
                        break;
                    case Card.CardTypes.Item:
                        HeroLine.Add(card);
                        var itemCard = card as ItemCard;
                        if (itemCard != null) itemCard.Icon.Show = true;
                        RearrangeZone(HeroLine,
                            _isHost ? GlobalVariables.P1HeroZoneDim : GlobalVariables.P2HeroZoneDim);
                        break;
                    case Card.CardTypes.Spell:
                        HeroLine.Add(card);
                        RearrangeZone(HeroLine,
                            _isHost ? GlobalVariables.P1HeroZoneDim : GlobalVariables.P2HeroZoneDim);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
                RearrangeZone(Resources, _isHost ? GlobalVariables.P1ResourceZoneDim : GlobalVariables.P2ResourceZoneDim);
                card.ToBoard();
            }
            else
                return false;
            return true;
        }

        public void PutToGraveyard(Card card)
        {
            Graveyard.Add(card);
            card.Dimension = _isHost
                ? new Rectangle(new Point(GlobalVariables.SpacesCard, GlobalVariables.P1AllyZoneDim.Y),
                    GlobalVariables.BoardCardDim)
                : new Rectangle(new Point(GlobalVariables.P2AllyZoneDim.X + GlobalVariables.P2AllyZoneDim.Width +
                                          GlobalVariables.DeckSpace, GlobalVariables.P2AllyZoneDim.Y),
                    GlobalVariables.BoardCardDim);
        }

        public void Discard(Card card)
        {
            card.TurnUp();
            Hand.Cards.Remove(card);
            PutToGraveyard(card);
            var unitCard = card as UnitCard;
            unitCard?.HideIcons();
            MessageHandler.SendMessage(GlobalVariables.Discarded, card);
        }

        private void RearrangeZone(List<Card> zone, Rectangle zoneDim)
        {
            var posX = zoneDim.X;
            var space = zoneDim.Width - (zone.FindAll(c => c.IsTapped).Count * GlobalVariables.BoardCardDim.Y +
                        zone.FindAll(c => !c.IsTapped).Count * GlobalVariables.BoardCardDim.X);
            if (space > GlobalVariables.BoardMaxSpace)
                space = GlobalVariables.BoardMaxSpace;
            foreach (var card in zone)
            {
                card.Dimension = new Rectangle(new Point(posX, zoneDim.Y + (card.IsTapped ? card.Dimension.Size.X / 2 - card.Dimension.Size.Y / 2 : 0)), card.Dimension.Size);
                posX += space + (card.IsTapped ? GlobalVariables.BoardCardDim.Y : GlobalVariables.BoardCardDim.X);
            }
        }

        private void RearrangeHand()
        {
            if(Hand.Cards.Count == 0)
                return;
            var posY = _isHost ? ScreenManager.Instance.Engine.GetResolution().Y - GlobalVariables.HandCardDim.Y/3 : -GlobalVariables.HandCardDim.Y/3*2;
            if (Hand.Cards.Count > 1)
            {
                var space = (ScreenManager.Instance.Engine.GetResolution().X -
                             GlobalVariables.HandCardDim.X * Hand.Cards.Count) / (Hand.Cards.Count + 2);
                if (space > GlobalVariables.HandMaxSpace)
                    space = GlobalVariables.HandMaxSpace;
                var posX = (ScreenManager.Instance.Engine.GetResolution().X -
                             GlobalVariables.HandCardDim.X * Hand.Cards.Count - space* (Hand.Cards.Count-1) )/2;
                foreach (var c in Hand.Cards)
                {
                    c.Dimension = new Rectangle(new Point(posX, posY),
                        GlobalVariables.HandCardDim);
                    posX += space + GlobalVariables.HandCardDim.X;
                }
            }
            else
            {
                Hand.Cards[0].Dimension = new Rectangle(new Point(ScreenManager.Instance.Engine.GetResolution().X/ 2- GlobalVariables.HandCardDim.X/2, posY),
                        GlobalVariables.HandCardDim);
            }
        }

        public void SetCards(List<Card> cards)
        {
            _cards = cards;
            Deck = new Deck{Cards = new Stack<Card>(_cards)};
            Deck.Shuffle();
        }

        public void Mulligan()
        {
            foreach (var card in Hand.Cards)
            {
                Deck.Cards.Push(card);
                card.Show = false;
            }
            Deck.Shuffle();
            Hand.Cards.Clear();
            FirstDraw();
        }
    }
}