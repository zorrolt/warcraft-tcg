﻿using System;
using System.Diagnostics;
using WarTcg.Game.Cards;

namespace WarTcg.Game
{
    public class SinglePlayerGameManager : GameManager
    {
        private readonly Ai _ai;

        public SinglePlayerGameManager()
        {
            _ai = new Ai();
            MessageHandler.SubscribeMessage(GlobalVariables.GameIsLoaded, (sender, args) => SetGameData(args as GameScreen.SetGameArgs));
            MessageHandler.SubscribeMessage(GlobalVariables.MulliganMessage, (sender, args) =>
            {
                Mulligan(args as GameScreen.MulliganArgs);
                Mulligan(new GameScreen.MulliganArgs { Mulligan = _ai.Mulligan(_p2Board.Hand.Cards), Player = GlobalVariables.Turn.Player2});
            });
            MessageHandler.SubscribeMessage(GlobalVariables.ActionStarted, (sender, args) => TurnStarted((args as PlayerArgs).Player));
            MessageHandler.SubscribeMessage(GlobalVariables.EndTurn, (sender, args) => NextTurn());
            MessageHandler.SubscribeMessage(GlobalVariables.CardZoomed, (sender, args) =>
            {
                MessageHandler.SendMessage(GlobalVariables.EnableEndTurn, this, new MainScene.EnableEndTurnArgs{ Enable = false});
            });
            MessageHandler.SubscribeMessage(GlobalVariables.CardZoomedOut, (sender, args) => MessageHandler.SendMessage(GlobalVariables.EnableEndTurn, this, new MainScene.EnableEndTurnArgs { Enable = Turn == GlobalVariables.Turn.Player }));
            MessageHandler.SubscribeMessage(GlobalVariables.CardClicked, (sender, args) => OnCardClick(sender, args as Card.CardEventArgs));
            MessageHandler.SubscribeMessage(GlobalVariables.CardRightClicked, (sender, args) => OnCardRightClick(sender, args as Card.CardEventArgs));
        }

        private void TurnStarted(GlobalVariables.Turn player)
        {

            Debug.WriteLine("Turn started: " + player);

            if (GlobalVariables.Turn.Player2 == player) 
            {
                _ai.Play(_p2Board, _p1Board);
                MessageHandler.SendMessage(GlobalVariables.EnableEndTurn, this, new MainScene.EnableEndTurnArgs { Enable = true });
            }
        }

        private void SetGameData(GameScreen.SetGameArgs data)
        {
            Debug.WriteLine("Set game data: \n\t * Enemy hero - " + data.EnemyHero.Name + "\n\t * Player hero - " + data.PlayerHero.Name);
            SetData(data.First, data.PlayerDeck, data.EnemyDeck, data.PlayerHero, data.EnemyHero);
        }

        private void Mulligan(GameScreen.MulliganArgs data)
        {
        Debug.WriteLine("Mulligan: " + data.Mulligan + " - " + data.Player);
            if(data.Mulligan)
                Mulligan(data.Player);
            else
                SkipMulligan(data.Player);
        }

        private void OnCardRightClick(object sender, Card.CardEventArgs cardArgs)
        {
            Debug.WriteLine("Card right-clicked: " + cardArgs.Card.Name + "\t by " + sender);
            SetResource(cardArgs.Card);
        }

        private void OnCardClick(object sender, Card.CardEventArgs cardArgs)
        {
            Debug.WriteLine("Card clicked: " + cardArgs.Card.Name + "\t by " + sender);
            if (sender == _ai && Turn == GlobalVariables.Turn.Player2 ||
                sender != _ai && Turn == GlobalVariables.Turn.Player)
            {
                if (Phase == TurnPhase.Action)
                {
                    switch (ActionState)
                    {
                        case ActionPhaseState.Normal:
                            if (!SetCard(cardArgs.Card) && !Battle(cardArgs.Card) && cardArgs.Card is ItemCard)
                               UseItem(cardArgs.Card as ItemCard);
                            break;
                        case ActionPhaseState.Effect:
                            break;
                        case ActionPhaseState.Battle:
                            if(IsAttacker(cardArgs.Card))
                                CancelBattle();
                            else
                                Attack(cardArgs.Card);
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }
                else if (Phase == TurnPhase.End)
                {

                }
            }

        }

    }
}