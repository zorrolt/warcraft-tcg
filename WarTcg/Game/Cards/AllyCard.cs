﻿using System;
using EngineWar.GUI;
using Microsoft.Xna.Framework;
using WarTcg.DataEntities.CardData;

namespace WarTcg.Game.Cards
{
    public class AllyCard : UnitCard
    {
        public AllyCard(Picture picture, string back, AllyData data) : base(picture, back, data, CardTypes.Ally)
        {
            Attack = data.Atk;
            AttackType = data.AtkType;
            AttackNum.Text = Attack.ToString();
            AttackNum.Show = false;
            switch (AttackType)
            {
                case CardBaseData.AttackType.Mele:
                    AttackIco = new Picture(new Rectangle(new Point(), GlobalVariables.WeaponIcoSize), GlobalVariables.MeleIcon) {Show = false};
                    break;
                case CardBaseData.AttackType.Range:
                    AttackIco = new Picture(new Rectangle(new Point(), GlobalVariables.WeaponIcoSize), GlobalVariables.MeleIcon) { Show = false };
                    break;
                case CardBaseData.AttackType.Fire:
                    AttackIco = new Picture(new Rectangle(new Point(), GlobalVariables.WeaponIcoSize), GlobalVariables.MeleIcon) { Show = false };
                    break;
                case CardBaseData.AttackType.Shadow:
                    AttackIco = new Picture(new Rectangle(new Point(), GlobalVariables.WeaponIcoSize), GlobalVariables.MeleIcon) { Show = false };
                    break;
                case CardBaseData.AttackType.Arcane:
                    AttackIco = new Picture(new Rectangle(new Point(), GlobalVariables.WeaponIcoSize), GlobalVariables.MeleIcon) { Show = false };
                    break;
                case CardBaseData.AttackType.Frost:
                    AttackIco = new Picture(new Rectangle(new Point(), GlobalVariables.WeaponIcoSize), GlobalVariables.MeleIcon) { Show = false };
                    break;
                case CardBaseData.AttackType.Holy:
                    AttackIco = new Picture(new Rectangle(new Point(), GlobalVariables.WeaponIcoSize), GlobalVariables.MeleIcon) { Show = false };
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public readonly CardBaseData.AttackType AttackType;

    }
}