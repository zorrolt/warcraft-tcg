﻿using EngineWar.GUI;
using WarTcg.DataEntities.CardData;

namespace WarTcg.Game.Cards
{
    public class QuestCard : Card
    {
        public QuestCard(Picture picture, string back, CardBaseData data) : base(picture, back, data, CardTypes.Quest)
        {
        }
    }
}