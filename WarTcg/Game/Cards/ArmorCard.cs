﻿using EngineWar.GUI;
using Microsoft.Xna.Framework;
using WarTcg.DataEntities.CardData;

namespace WarTcg.Game.Cards
{
    public class ArmorCard : ItemCard
    {
        public ArmorCard(Picture picture, string back, ArmorData data) : base(picture, back, data)
        {
            Type = data.Type;
            Icon = new Picture(new Rectangle(new Point(), GlobalVariables.ArmorIcoSize), GlobalVariables.ArmorIcon)
            {
                Show = false
            };
        }

        public readonly ArmorData.ArmorType Type;
    }
}