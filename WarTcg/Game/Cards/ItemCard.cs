﻿using EngineWar.GUI;
using Microsoft.Xna.Framework;
using WarTcg.DataEntities.CardData;

namespace WarTcg.Game.Cards
{
    public abstract class ItemCard : Card
    {
        public readonly TextBox DurabilityNum;
        public Picture Icon { get; protected set; }

        protected ItemCard(Picture picture, string back, ItemData data) : base(picture, back, data, CardTypes.Item)
        {
            Classes = data.Classes;
            Slot = data.Slot;
            Durability = data.Durability;
            DurabilityNum = new TextBox(new Rectangle(), Durability.ToString()) { Show = false, FontColor = Color.White, AutoSize = true, FontSize = 0.5f, Align = TextBox.AlignType.Center };
            DurabilityNum.SetBackgroundColor(Color.Transparent);
            OnZoom = () =>
            {
                Icon.Show = false;
                DurabilityNum.Show = false;
            };
            OnZoomOut = () =>
            {
                Icon.Show = true;
                DurabilityNum.Show = true;
            };
        }

        public bool IsBroken => Durability < 0;

        public int DoDamage(int damage)
        {
            var overDamage = damage - Durability;
            Durability -= damage;
            if (Durability < 0)
                Durability = 0;
            DurabilityNum.Text = Durability.ToString();
            return overDamage > -1 ? overDamage : 0;
        }

        public int Durability { get; private set; }

        public override Rectangle Dimension
        {
            get { return base.Dimension; }
            set
            {
                base.Dimension = value;
                RearrangeIcons();
            }
        }

        public override void SetTapped(bool isTapped)
        {
            base.SetTapped(isTapped);
            RearrangeIcons();
        }

        public override void TurnDown()
        {
            base.TurnDown();
            Icon.Show = false;
            DurabilityNum.Show = false;
        }

        public override bool Show
        {
            get { return base.Show; }
            set
            {
                base.Show = value;
                Icon.Show = value;
                DurabilityNum.Show = value;
            }
        }

        private void RearrangeIcons()
        {
            if (IsTapped)
            {
                Icon.Dimension =
                    new Rectangle(
                        new Point(Dimension.X + Dimension.Width - Icon.Dimension.Width + 2,
                            Dimension.Y + 1), Icon.Dimension.Size);
                DurabilityNum.Dimension = new Rectangle(Icon.Dimension.Location, Icon.Dimension.Size);
            }
            else
            {
                Icon.Dimension =
                    new Rectangle(
                        new Point(Dimension.X + Dimension.Width - Icon.Dimension.Width - 2,
                            Dimension.Y + Dimension.Height - Icon.Dimension.Height + 1), Icon.Dimension.Size);
                DurabilityNum.Dimension = new Rectangle(Icon.Dimension.Location, Icon.Dimension.Size);
            }
        }
        public readonly CardBaseData.ClassType[] Classes;
        public readonly ItemData.SlotType Slot;
    }
}