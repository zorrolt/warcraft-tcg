﻿using System;
using System.Collections.Generic;
using EngineWar;
using EngineWar.GUI;
using Microsoft.Xna.Framework;
using WarTcg.DataEntities.CardData;

namespace WarTcg.Game.Cards
{
    public abstract class Card
    {
        public enum CardTypes
        {
            Quest, Ally, Hero, Item, Spell
        }

        public class CardEventArgs : EventArgs
        {
            public Card Card;
        }

        public const Scene.Layers BoardCardsLayer = Scene.Layers.Third;
        public const Scene.Layers HandCardsLayer = Scene.Layers.Second;
        public const Scene.Layers ZoomCardsLayer = Scene.Layers.Top;

        public Card AttachedTo { get; set; }
        public readonly Guid Id;
        public readonly CardTypes CardType;
        public bool Facedown { get; private set; }
        public bool IsTapped { get; private set; }
        private readonly string _content, _back;
        public string Name;
        public string Description;
        private readonly Picture _picture;
        public int Cost;
        private Point _lastPos = Point.Zero;
        public bool IsZoomed { get; private set; }
        private Point _lastDim;
        private Scene.Layers _lastLayer;

        public virtual void SetTapped(bool isTapped)
        {
            if (isTapped == IsTapped)
                return;
            _picture.Turn90();
            IsTapped = isTapped;
        }

        protected Action OnZoomOut = null, OnZoom = null;

        public virtual void ToBoard()
        {
            _picture.ToLayer(BoardCardsLayer);
        }

        public virtual void ToHand()
        {
            _picture.ToLayer(HandCardsLayer);
        }

        private void Middle()
        {
            if(Facedown)
                return;

            IsZoomed = !IsZoomed;

            if (IsZoomed)
            {
                _lastLayer = _picture.Layer;
                _picture.ToLayer(ZoomCardsLayer);
                _lastDim = _picture.Dimension.Size;
                if (IsTapped)
                {
                    _picture.Turn90();
                    _lastDim = new Point(_lastDim.Y, _lastDim.X);
                }
                _lastPos = _picture.Dimension.Location;
                var center = new Point(ScreenManager.Instance.Engine.GetResolution().X / 2 - GlobalVariables.CardZoomedDim.X / 2,
                    ScreenManager.Instance.Engine.GetResolution().Y / 2 - GlobalVariables.CardZoomedDim.Y / 2);
                _picture.Dimension = new Rectangle(center, GlobalVariables.CardZoomedDim);
                MessageHandler.SendMessage(GlobalVariables.CardZoomed, this);
                OnZoom?.Invoke();
            }
            else
            {
                _picture.ToLayer(_lastLayer);
                _picture.Dimension = new Rectangle(_lastPos, _lastDim);
                if (IsTapped)
                {
                    _picture.Turn90();
                }
                MessageHandler.SendMessage(GlobalVariables.CardZoomedOut);
                OnZoomOut?.Invoke();
            }
        }

        protected Card(Picture picture, string backName, CardBaseData data, CardTypes cardType)
        {
            Effects = data.Effect;
            Id = Guid.NewGuid();
            CardType = cardType;
            _picture = picture;
            _back = backName;
            Name = data.Name;
            Cost = data.Cost;
            Description = data.Text;
            _content = _picture.GetTextureName;
            _picture.OnMiddle = Middle;
            _picture.OnClick = () => MessageHandler.SendMessage(GlobalVariables.CardClicked, this, new CardEventArgs{Card = this});
            _picture.OnHover = () => MessageHandler.SendMessage(GlobalVariables.CardHover, this);
            _picture.OnLeave = () => MessageHandler.SendMessage(GlobalVariables.CardLeave, this);
            _picture.OnClickRight = () => MessageHandler.SendMessage(GlobalVariables.CardRightClicked, this, new CardEventArgs { Card = this });
        }

        public readonly List<CardBaseData.CardEffectData> Effects;

        public Picture GetPicture()
        {
            return _picture;
        }

        public virtual void TurnDown()
        {
            _picture.SetTextureName(_back);
            Facedown = true;
        }

        public virtual Rectangle Dimension
        {
            set { _picture.Dimension = value; }
            get { return _picture.Dimension; }
        }

        public virtual bool Show { get { return _picture.Show; } set { _picture.Show = value; } }

        public virtual void TurnUp()
        {
            _picture.SetTextureName(_content);
            Facedown = false;
        }
    }
}