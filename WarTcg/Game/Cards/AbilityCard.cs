﻿using EngineWar.GUI;
using WarTcg.DataEntities.CardData;

namespace WarTcg.Game.Cards
{
    public class AbilityCard : Card
    {
        public AbilityCard(Picture picture, string back, AbilityData data) : base(picture, back, data, CardTypes.Spell)
        {
            Type = data.Type;
            SubType = data.SubType;
            Class = data.Class;
            SubClass = data.SubClass;
        }
        public readonly AbilityData.AbilitySubType SubType;
        public readonly AbilityData.AbilityType Type;
        public readonly CardBaseData.ClassType Class;
        public readonly CardBaseData.SubClassType SubClass;
    }
}