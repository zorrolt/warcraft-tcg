﻿using EngineWar.GUI;
using Microsoft.Xna.Framework;
using WarTcg.DataEntities.CardData;

namespace WarTcg.Game.Cards
{
    public class WeaponCard : ItemCard
    {
        public WeaponCard(Picture picture, string back, WeaponData data) : base(picture, back, data)
        {
            Atk = data.Atk;
            AtkType = data.AtkType;
            Type = data.Type;
            Icon = new Picture(new Rectangle(new Point(), GlobalVariables.WeaponIcoSize), GlobalVariables.WeaponIcon)
            {
                Show = false
            };
        }

        public readonly int Atk;
        public readonly CardBaseData.AttackType AtkType;
        public readonly WeaponData.WeaponType Type;
    }
}