﻿using System;
using EngineWar.GUI;
using WarTcg.DataEntities.CardData;

namespace WarTcg.Game.Cards
{
    public class HeroCard : UnitCard
    {
        private readonly Random _random;
        public HeroCard(Picture picture, string back, HeroData data) : base(picture, back, data, CardTypes.Hero)
        {
            Profession1 = data.Prof1;
            Profession2 = data.Prof2;
            SubClass = data.SubClass;
            _random = new Random();
        }

        public ArmorCard Head, Hands, Legs, Chest, Waist, Feet;
        public WeaponCard Weapon;

        // TODO: more slots
        public override void DoDamage(int damage)
        {
            var d = damage;
            var r = _random.Next(3);
            if (r == 0)
            {
                if (Head != null)
                {
                    d = Head.DoDamage(damage);
                    if (Head.IsBroken)
                        Head = null;
                }
            }
            else if (r == 1)
            {
                if (Hands != null)
                {
                    d = Hands.DoDamage(damage);
                    if (Hands.IsBroken)
                        Hands = null;
                }

            }
            base.DoDamage(d);
            if (Health < 1)
                MessageHandler.SendMessage(GlobalVariables.HeroDied, this);
        }

        public readonly HeroData.Profession Profession1, Profession2;
        public readonly CardBaseData.SubClassType SubClass;
    }
}
