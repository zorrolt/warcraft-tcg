﻿using EngineWar.GUI;
using Microsoft.Xna.Framework;
using WarTcg.DataEntities.CardData;

namespace WarTcg.Game.Cards
{
    public abstract class UnitCard : Card
    {
        protected UnitCard(Picture picture, string back, UnitCardData data, CardTypes cardType)
            : base(picture, back, data, cardType)
        {
            Attack = 0;
            Health = data.Health;
            Faction = data.Faction;
            Class = data.Class;
            Race = data.Race;
            HealthIco = new Picture(new Rectangle(new Point(), GlobalVariables.HealthIcoSize), GlobalVariables.HealthIcon)
            {
                Show = false
            };
            AttackNum = new TextBox(new Rectangle(), Health.ToString()) { Show = false, FontColor = Color.White, Align = TextBox.AlignType.Center, FontName = GlobalVariables.BoardFontName};
            AttackNum.SetBackgroundColor(Color.Transparent);
            HealthNum = new TextBox(new Rectangle(), Health.ToString()) {Show = false, FontColor = Color.White, Align = TextBox.AlignType.Center, FontName = GlobalVariables.BoardFontName };
            HealthNum.SetBackgroundColor(Color.Transparent);
            OnZoom = () => 
            {
                if (Health > 0)
                {
                    HealthIco.ToLayer(ZoomCardsLayer);
                    HealthNum.ToLayer(ZoomCardsLayer);
                    if (AttackIco != null)
                    {
                        AttackIco.ToLayer(ZoomCardsLayer);
                        AttackNum.ToLayer(ZoomCardsLayer);
                    }
                    HealthIco.Dimension = new Rectangle(Dimension.Location + new Point(Dimension.Width, Dimension.Height - HealthIco.Dimension.Size.Y*GlobalVariables.ZoomCardMulti), HealthIco.Dimension.Size*new Point(GlobalVariables.ZoomCardMulti));
                    HealthNum.Dimension = new Rectangle(HealthIco.Dimension.Location - new Point(15, 0), HealthNum.Dimension.Size * new Point(GlobalVariables.ZoomCardMulti));
                    HealthNum.FontName = GlobalVariables.MenuFontName;
                    if (AttackIco == null)
                        return;
                    AttackIco.Dimension = new Rectangle(Dimension.Location + new Point(-AttackIco.Dimension.Width*GlobalVariables.ZoomCardMulti, Dimension.Height - AttackIco.Dimension.Size.Y* GlobalVariables.ZoomCardMulti), AttackIco.Dimension.Size * new Point(GlobalVariables.ZoomCardMulti));
                    AttackNum.Dimension = new Rectangle(AttackIco.Dimension.Location - new Point(15, 0), AttackNum.Dimension.Size * new Point(GlobalVariables.ZoomCardMulti));
                    AttackNum.FontName = GlobalVariables.MenuFontName;
                }
            };
            OnZoomOut = () =>
            {
                if (Health > 0)
                {
                    HealthIco.ToLayer(BoardCardsLayer + 1);
                    HealthNum.ToLayer(BoardCardsLayer + 1);
                    if (AttackIco != null)
                    {
                        AttackIco.ToLayer(BoardCardsLayer + 1);
                        AttackNum.ToLayer(BoardCardsLayer + 1);
                    }
                    HealthIco.Dimension = new Rectangle(HealthIco.Dimension.Location, HealthIco.Dimension.Size/new Point(GlobalVariables.ZoomCardMulti));
                    HealthNum.Dimension = new Rectangle(HealthNum.Dimension.Location, HealthNum.Dimension.Size / new Point(GlobalVariables.ZoomCardMulti));
                    HealthNum.FontName = GlobalVariables.BoardFontName;
                    if (AttackIco != null)
                    {
                        AttackIco.Dimension = new Rectangle(AttackIco.Dimension.Location,
                            AttackIco.Dimension.Size / new Point(GlobalVariables.ZoomCardMulti));
                        AttackNum.Dimension = new Rectangle(AttackNum.Dimension.Location, AttackNum.Dimension.Size / new Point(GlobalVariables.ZoomCardMulti));
                        AttackNum.FontName = GlobalVariables.BoardFontName;
                    }
                    RearrangeIcons();
                }
            };
        }
        public int Attack { get; protected set; }
        public readonly Picture HealthIco;
        public Picture AttackIco { get; protected set; }
        public readonly TextBox HealthNum, AttackNum;
        public bool IsDead => Health <= 0;

        public void HideIcons()
        {
            HealthIco.Show = false;
            HealthNum.Show = false;
            if (AttackIco != null)
            {
                AttackIco.Show = false;
                AttackNum.Show = false;
            }
        }

        public virtual void DoDamage(int damage)
        {
            Health -= damage;
            HealthNum.Text = Health.ToString();
            if (IsDead)
            {
                HealthIco.Show = false;
                HealthNum.Show = false;
                if (AttackIco != null)
                {
                    AttackIco.Show = false;
                    AttackNum.Show = false;
                }
                MessageHandler.SendMessage(GlobalVariables.CardDied, this);
            }
        }

        public override void ToHand()
        {
            base.ToHand();
            HealthIco.ToLayer(HandCardsLayer + 1);
            HealthNum.ToLayer(HandCardsLayer + 1);
            AttackIco?.ToLayer(HandCardsLayer + 1);
            AttackNum?.ToLayer(HandCardsLayer + 1);
        }


        public override void ToBoard()
        {
            base.ToBoard();
            HealthIco.ToLayer(BoardCardsLayer + 1);
            HealthNum.ToLayer(BoardCardsLayer + 1);
            AttackIco?.ToLayer(BoardCardsLayer + 1);
            AttackNum?.ToLayer(BoardCardsLayer + 1);
        }

        public override void SetTapped(bool isTapped)
        {
            base.SetTapped(isTapped);
            RearrangeIcons();
        }

        public override void TurnDown()
        {
            base.TurnDown();
            HealthIco.Show = false;
            HealthNum.Show = false;
            if (AttackIco != null)
            {
                AttackIco.Show = false;
                AttackNum.Show = false;
            }
        }

        public override void TurnUp()
        {
            base.TurnUp();
            HealthIco.Show = true;
            HealthNum.Show = true;
            if (AttackIco != null)
            {
                AttackIco.Show = true;
                AttackNum.Show = true;
            }
        }

        public override bool Show
        {
            get { return base.Show; }
            set
            {
                base.Show = value;
                if(Facedown)
                    return;
                HealthIco.Show = value;
                HealthNum.Show = value;
                if (AttackIco != null)
                {
                    AttackIco.Show = value;
                    AttackNum.Show = value;
                }
            }
        }

        private void RearrangeIcons()
        {
            if (IsTapped)
            {
                HealthIco.Dimension =
                    new Rectangle(
                        new Point(Dimension.X + Dimension.Width - HealthIco.Dimension.Width + 2,
                            Dimension.Y + 1), GlobalVariables.HealthIcoSize);
                HealthNum.Dimension = new Rectangle(HealthIco.Dimension.Location - new Point(5, 0), GlobalVariables.CardHealthTextboxSize);
                if (AttackIco == null)
                    return;
                AttackIco.Dimension =
                    new Rectangle(
                        new Point(Dimension.X + Dimension.Width - AttackIco.Dimension.Width + 2,
                            Dimension.Y + Dimension.Height - AttackIco.Dimension.Height + 1), GlobalVariables.ArmorIcoSize);
                AttackNum.Dimension = new Rectangle(AttackIco.Dimension.Location - new Point(5, 0), GlobalVariables.CardAttackTextboxSize);
            }
            else
            {
                HealthIco.Dimension =
                    new Rectangle(
                        new Point(Dimension.X + Dimension.Width - HealthIco.Dimension.Width - 2,
                            Dimension.Y + Dimension.Height - HealthIco.Dimension.Height + 1), GlobalVariables.HealthIcoSize);
                HealthNum.Dimension = new Rectangle(HealthIco.Dimension.Location - new Point(5, 0), GlobalVariables.CardHealthTextboxSize);
                if(AttackIco == null)
                    return;
                AttackIco.Dimension =
                    new Rectangle(
                        new Point(Dimension.X + 2,
                            Dimension.Y + Dimension.Height - AttackIco.Dimension.Height + 1), GlobalVariables.ArmorIcoSize);
                AttackNum.Dimension = new Rectangle(AttackIco.Dimension.Location - new Point(5, 0), GlobalVariables.CardAttackTextboxSize);
            }
        }

        public override Rectangle Dimension
        {
            get { return base.Dimension; }
            set
            {
                base.Dimension = value;
                RearrangeIcons();
            }
        }

        public readonly UnitCardData.FactionType Faction;
        public readonly CardBaseData.ClassType Class;
        public readonly UnitCardData.RaceType Race;
        public int Health { get; private set; }
    }
}