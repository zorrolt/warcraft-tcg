﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using WarTcg.DataEntities.CardData;
using WarTcg.Game.Cards;

namespace WarTcg.Game
{
    public abstract class GameManager
    {
        public class ReadyArgs : PlayerArgs
        {
            public bool Mulligan;
        }

        public class PlayerArgs : EventArgs
        {
            public GlobalVariables.Turn Player;
        }

        public enum TurnPhase
        {
            GameStart, Begin, Action, End
        }

        private bool IsDataSet { get; set; }
        private UnitCard _attacker;
        private bool _isPlayer1Ready, _isPlayer2Ready;
        protected GlobalVariables.Turn Turn { get; private set; }
        private bool _isFirstTurn = true;
        protected TurnPhase Phase { get; private set; }
        protected ActionPhaseState ActionState { get; private set; }
        protected Board _p1Board, _p2Board;
        private bool IsResourceSet { set; get; }

        public enum ActionPhaseState
        {
            Normal, Effect, Battle
        }

        protected GameManager()
        {
            IsDataSet = false;
            Phase = TurnPhase.GameStart;
        }

        private void NextPhase()
        {
            if(!IsDataSet || Phase == TurnPhase.Action && ActionState != ActionPhaseState.Normal)
                return;

            if (Phase != TurnPhase.End)
                Phase++;
            else
            {
                Phase = TurnPhase.Begin;
                Turn = Turn == GlobalVariables.Turn.Player2 ? GlobalVariables.Turn.Player : GlobalVariables.Turn.Player2;
            }
            Debug.WriteLine("Game phase:\t " + Phase + (Phase == TurnPhase.Action ? " - "  + ActionState : ""));
            PhaseActions();
        }

        private void PhaseActions()
        {
            switch (Phase)
            {
                case TurnPhase.GameStart:
                    break;
                case TurnPhase.Begin:
                    Begin();
                    break;
                case TurnPhase.Action:
                    Act();
                    break;
                case TurnPhase.End:
                    End();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void Begin()
        {
            MessageHandler.SendMessage(GlobalVariables.TurnStarted, this, new PlayerArgs { Player = Turn });
            CheckOngoingEffects();
            var board = Turn == GlobalVariables.Turn.Player ? _p1Board : _p2Board;
            board.Untap();
            if (_isFirstTurn)
                _isFirstTurn = false;
            else
            {
                if (board.Deck.Cards.Count == 0)
                {
                    MessageHandler.SendMessage(GlobalVariables.GameEnded, this, new PlayerArgs {Player =  Turn == GlobalVariables.Turn.Player ? GlobalVariables.Turn.Player : GlobalVariables.Turn.Player2 });
                    IsDataSet = false;
                    return;
                }
                board.Draw();
            }
            
            NextPhase();
        }

        protected bool Battle(Card attacker)
        {
            if (Phase == TurnPhase.Action && ActionState == ActionPhaseState.Normal && !attacker.IsTapped && ValidateAttacker(attacker))
            {
                Debug.WriteLine("Attacking: " + attacker.Name);
                ActionState = ActionPhaseState.Battle;
                _attacker = attacker as UnitCard;
                _attacker.SetTapped(true);
                MessageHandler.SendMessage(GlobalVariables.BattleStarted);
                return true;
            }
            return false;
        }

        protected void UseItem(ItemCard item)
        {
            var board = Turn == GlobalVariables.Turn.Player ? _p1Board : _p2Board;
            if (Phase == TurnPhase.Action && ActionState == ActionPhaseState.Normal && ValidateItem(item) && !item.IsBroken)
            {
                if (item is ArmorCard)
                {
                    switch (item.Slot)
                    {
                        case ItemData.SlotType.Head:
                            CheckAndEquip(board.Hero.Head, (ArmorCard)item);
                            break;
                        case ItemData.SlotType.Hands:
                            CheckAndEquip(board.Hero.Head, (ArmorCard)item);
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }
                else if (item is WeaponCard)
                {
                    if (board.Hero.Weapon == null)
                    {
                        board.Hero.Weapon = (WeaponCard) item;
                        item.SetTapped(true);
                        Debug.WriteLine("Weapon equipped: " + item.Name);
                    }
                    else if (board.Hero.Weapon == item)
                    {
                        board.Hero.Weapon = null;
                        item.SetTapped(false);
                        Debug.WriteLine("Weapon unequipped: " + item.Name);
                    }
                }
            }
        }

        private void CheckAndEquip(ArmorCard slot, ArmorCard item)
        {
            if(item.IsBroken)
                return;
            if (slot == null)
            {
                slot = item;
                slot.SetTapped(true);
                Debug.WriteLine("Item equipped: " + item.Name);
            }
            else if (slot == item)
            {
                slot = null;
                item.SetTapped(false);
                Debug.WriteLine("Item unequipped: " + item.Name);
            }
        }

        protected void CancelBattle()
        {
            Debug.WriteLine("Canceled attacking: " + _attacker.Name);
            ActionState = ActionPhaseState.Normal;
            _attacker.SetTapped(false);
            _attacker = null;
            MessageHandler.SendMessage(GlobalVariables.BattleEnded);
        }

        protected bool IsAttacker(Card attacker)
        {
            return _attacker == attacker;
        }

        private bool ValidateAttacker(Card card)
        {
            var board = Turn == GlobalVariables.Turn.Player ? _p1Board : _p2Board;
            return board.Allies.Contains(card) || board.Hero == card && board.Hero.Weapon != null;
        }

        private bool ValidateItem(ItemCard item)
        {
            return Turn == GlobalVariables.Turn.Player && _p1Board.HeroLine.Contains(item) ||
                   Turn == GlobalVariables.Turn.Player2 && _p2Board.HeroLine.Contains(item);
        }

        private void Act()
        {
            ActionState = ActionPhaseState.Normal;
            IsResourceSet = false;
            MessageHandler.SendMessage(GlobalVariables.ActionStarted, this, new PlayerArgs { Player = Turn });
        }

        public void SetData(GlobalVariables.Turn first, List<Card> playerCards, List<Card> player2Cards, HeroCard hero1, HeroCard hero2)
        {
            IsResourceSet = false;
            _isPlayer1Ready = false;
            _isPlayer2Ready = false;
            Phase = TurnPhase.GameStart;
            Turn = first;
            _p1Board = new Board(playerCards, true, hero1);
            _p2Board = new Board(player2Cards, false, hero2);
            _p1Board.Deck.Shuffle();
            _p2Board.Deck.Shuffle();
            _p1Board.FirstDraw();
            _p2Board.FirstDraw();
            IsDataSet = true;
        }

        protected void End()
        {
            if (Turn == GlobalVariables.Turn.Player2 && _p2Board.Hand.Cards.Count > GlobalVariables.HandCardsCount ||
                Turn == GlobalVariables.Turn.Player && _p1Board.Hand.Cards.Count > GlobalVariables.HandCardsCount)
            {
                MessageHandler.SendMessage(GlobalVariables.Discard, this, new PlayerArgs { Player = Turn });
                return;
            }
            MessageHandler.SendMessage(GlobalVariables.TurnEnded);
            NextPhase();
        }

        private void ExecuteOngoingEffect(CardBaseData.CardEffectData effect, Card card)
        {
            switch (effect.Type)
            {
                case CardBaseData.CardEffectData.EffectType.Attach:
                    break;
                case CardBaseData.CardEffectData.EffectType.DoDamage:
                    if(effect.TargetType == CardBaseData.CardEffectData.EffectTargetType.Attached && (card.AttachedTo.CardType == Card.CardTypes.Ally || card.AttachedTo.CardType == Card.CardTypes.Hero))
                        (card.AttachedTo as UnitCard).DoDamage(effect.EffectValue);
                    break;
                case CardBaseData.CardEffectData.EffectType.Target:
                    break;
                case CardBaseData.CardEffectData.EffectType.Active:
                    break;
                case CardBaseData.CardEffectData.EffectType.Destroy:
                    break;
                case CardBaseData.CardEffectData.EffectType.Buff:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void CheckOngoingEffects()
        {
            var board = Turn == GlobalVariables.Turn.Player ? _p1Board : _p2Board;
            foreach (var ability in board.HeroLine)
            {
                if (ability.CardType == Card.CardTypes.Spell)
                {
                    foreach (var effect in ability.Effects)
                    {
                        if (effect.Ongoing)
                            ExecuteOngoingEffect(effect, ability);
                    }
                }
            }
        }

        protected void NextTurn()
        {
            End();
        }

        private void CheckIsGameReady()
        {
            if (_isPlayer1Ready && _isPlayer2Ready)
                NextPhase();
        }

        protected void SetResource(Card card)
        {
            if(IsResourceSet)
                return;
            var bord = Turn == GlobalVariables.Turn.Player ? _p1Board : _p2Board;
            if (bord.PutAsResource(card))
                IsResourceSet = true;
        }

        private bool ValidateDeffender(UnitCard card)
        {
            return Turn == GlobalVariables.Turn.Player && (_p2Board.Allies.Contains(card) || _p2Board.Hero == card) || Turn == GlobalVariables.Turn.Player2 && (_p1Board.Allies.Contains(card) || _p1Board.Hero == card);
        }

        protected void Attack(Card deffender)
        {
            var unit = deffender as UnitCard;
            if(unit == null || !ValidateDeffender(unit))
                return;
            Debug.WriteLine("Defending: " + unit.Name);
            if (_attacker.CardType == Card.CardTypes.Hero && (_attacker as HeroCard).Weapon != null && !(_attacker as HeroCard).Weapon.IsBroken)
            {
                unit.DoDamage((_attacker as HeroCard).Weapon.Atk);
                (_attacker as HeroCard).Weapon.DoDamage(GlobalVariables.DamageToWeapon);
            }
            else
                unit.DoDamage(_attacker.Attack);
            if (deffender.CardType == Card.CardTypes.Hero && (deffender as HeroCard).Weapon != null && !(deffender as HeroCard).Weapon.IsBroken)
            {
                _attacker.DoDamage((deffender as HeroCard).Weapon.Atk);
                (deffender as HeroCard).Weapon.DoDamage(GlobalVariables.DamageToWeapon);
            }
            else
                _attacker.DoDamage(unit.Attack);
            _attacker = null;
            ActionState = ActionPhaseState.Normal;
            MessageHandler.SendMessage(GlobalVariables.BattleEnded);
        }

        public bool IsInHand(GlobalVariables.Turn player, Card card)
        {
            if (player == GlobalVariables.Turn.Player2)
                return _p2Board.Hand.Cards.Contains(card);
            return _p1Board.Hand.Cards.Contains(card);
        }

        protected void ExecuteEffect()
        {
            
        }

        protected bool SetCard(Card card)
        {
            if (card is QuestCard && IsResourceSet)
                return false;
            if (Turn == GlobalVariables.Turn.Player)
            {
                if (_p1Board.Put(card))
                {
                    if (card is QuestCard)
                    {
                        IsResourceSet = true;
                        Debug.WriteLine("Resource is set");
                    }
                    else if (card.CardType == Card.CardTypes.Spell && card.Effects.Count > 0)
                    {
                        ActionState = ActionPhaseState.Effect;
                        ExecuteEffect();
                    }
                    return true;
                }
            }
            else if (_p2Board.Put(card) && card is QuestCard)
            {
                IsResourceSet = true;
                return true;
            }
            return false;
        }

        protected void Mulligan(GlobalVariables.Turn player)
        {
            if (!_isPlayer2Ready && GlobalVariables.Turn.Player2 == player)
            {
                _p2Board.Mulligan();
                _isPlayer2Ready = true;
            }
            else if(!_isPlayer1Ready && GlobalVariables.Turn.Player == player)
            {
                _p1Board.Mulligan();
                _isPlayer1Ready = true;
            }
            CheckIsGameReady();
        }

        protected void SkipMulligan(GlobalVariables.Turn player)
        {
            if (player == GlobalVariables.Turn.Player2)
                _isPlayer2Ready = true;
            else
                _isPlayer1Ready = true;

            CheckIsGameReady();
        }
    }
}