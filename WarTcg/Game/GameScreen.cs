﻿using System;
using System.Collections.Generic;
using EngineWar;
using EngineWar.DataEntities;
using Microsoft.Xna.Framework.Input;
using WarTcg.Game.Cards;

namespace WarTcg.Game
{
    internal class GameScreen : Screen
    {
        public class MessageArgs : EventArgs
        {
            public string Text = "";
        }

        public class MulliganArgs : EventArgs
        {
            public bool Mulligan;
            public GlobalVariables.Turn Player;
        }

        public class SetGameArgs : EventArgs
        {
            public GlobalVariables.Turn First;
            public List<Card> EnemyDeck;
            public List<Card> PlayerDeck;
            public HeroCard EnemyHero;
            public HeroCard PlayerHero;
            public List<Content.ElementData> Content;
        }
        
        private MainScene _mainScene;

        public GameScreen() : base(GlobalVariables.GameScreenName, GlobalVariables.GameContentFile)
        {
            // Register game messages
            MessageHandler.RegisterMessage(GlobalVariables.EndTurn);
            MessageHandler.RegisterMessage(GlobalVariables.ShowMessage);
            MessageHandler.RegisterMessage(GlobalVariables.HideMessage);
            MessageHandler.RegisterMessage(GlobalVariables.TurnStarted);
            MessageHandler.RegisterMessage(GlobalVariables.ActionStarted);
            MessageHandler.RegisterMessage(GlobalVariables.TurnEnded);
            MessageHandler.RegisterMessage(GlobalVariables.Discard);
            MessageHandler.RegisterMessage(GlobalVariables.Discarded);
            MessageHandler.RegisterMessage(GlobalVariables.CardClicked);
            MessageHandler.RegisterMessage(GlobalVariables.CardRightClicked);
            MessageHandler.RegisterMessage(GlobalVariables.CardHover);
            MessageHandler.RegisterMessage(GlobalVariables.CardLeave);
            MessageHandler.RegisterMessage(GlobalVariables.CardDied);
            MessageHandler.RegisterMessage(GlobalVariables.BattleStarted);
            MessageHandler.RegisterMessage(GlobalVariables.BattleEnded);
            MessageHandler.RegisterMessage(GlobalVariables.CardZoomed);
            MessageHandler.RegisterMessage(GlobalVariables.CardZoomedOut);
            MessageHandler.RegisterMessage(GlobalVariables.HeroDied);
            MessageHandler.RegisterMessage(GlobalVariables.MulliganMessage);
            MessageHandler.RegisterMessage(GlobalVariables.EnableEndTurn);

            MessageHandler.SubscribeMessage(GlobalVariables.GameIsLoaded, (sender, args) => SetGameData(args as SetGameArgs));
            
        }

        public override void Initialize()
        {
            // Subscribe game messages
            MessageHandler.SubscribeMessage(GlobalVariables.BattleStarted, (sender, args) => SetBackground(GlobalVariables.BattleBackgroundName));
            MessageHandler.SubscribeMessage(GlobalVariables.BattleEnded, (sender, args) => SetBackground(GlobalVariables.NormalBackgroundName));

            // Setup Screen
            _mainScene = new MainScene();

            AddScene(_mainScene);
            SetScene(GlobalVariables.MainGameScene);
        }

        private void SetGameData(SetGameArgs data)
        {
            _mainScene.KeyMap.AddKey(Keys.Escape, () => MessageHandler.SendMessage(GlobalVariables.ToPauseMenu));
            AddTextures(data.Content);
            _mainScene.AddElement(Card.BoardCardsLayer, data.EnemyHero.GetPicture());
            _mainScene.AddElement(Card.BoardCardsLayer + 1, data.EnemyHero.HealthIco);
            _mainScene.AddElement(Card.BoardCardsLayer + 1, data.EnemyHero.HealthNum);
            data.EnemyHero.HealthIco.Show = true;
            foreach (var card in data.EnemyDeck)
            {
                _mainScene.AddElement(Card.HandCardsLayer, card.GetPicture());
                if (card is UnitCard)
                {
                    _mainScene.AddElement(Card.BoardCardsLayer + 1, (card as UnitCard).HealthIco);
                    _mainScene.AddElement(Card.BoardCardsLayer + 1, (card as UnitCard).AttackIco);
                    _mainScene.AddElement(Card.BoardCardsLayer + 1, (card as UnitCard).HealthNum);
                    _mainScene.AddElement(Card.BoardCardsLayer + 1, (card as UnitCard).AttackNum);
                }
                else if (card is ItemCard)
                {
                    _mainScene.AddElement(Card.BoardCardsLayer + 1, (card as ItemCard).Icon);
                   _mainScene.AddElement(Card.BoardCardsLayer + 1, (card as ItemCard).DurabilityNum);
                }
                card.TurnDown();
            }
            _mainScene.AddElement(Card.BoardCardsLayer, data.PlayerHero.GetPicture());
            _mainScene.AddElement(Card.BoardCardsLayer + 1, data.PlayerHero.HealthIco);
            _mainScene.AddElement(Card.BoardCardsLayer + 1, data.PlayerHero.HealthNum);
            data.PlayerHero.HealthIco.Show = true;
            foreach (var card in data.PlayerDeck)
            {
                _mainScene.AddElement(Card.HandCardsLayer, card.GetPicture());
                if (card is UnitCard)
                {
                    _mainScene.AddElement(Card.BoardCardsLayer + 1, (card as UnitCard).HealthIco);
                    _mainScene.AddElement(Card.BoardCardsLayer + 1, (card as UnitCard).HealthNum);
                    _mainScene.AddElement(Card.BoardCardsLayer + 1, (card as UnitCard).AttackIco);
                    _mainScene.AddElement(Card.BoardCardsLayer + 1, (card as UnitCard).AttackNum);
                }
                else if (card is ItemCard)
                {
                    _mainScene.AddElement(Card.BoardCardsLayer + 1, (card as ItemCard).Icon);
                    _mainScene.AddElement(Card.BoardCardsLayer + 1, (card as ItemCard).DurabilityNum);
                }
                card.TurnDown();
            }
            _mainScene.SetDialog(data.First);
        }

        public override void OnLoad()
        {
            _mainScene.OnLoad();
        }

        public override void OnUnload()
        {
            
        }
    }
}
