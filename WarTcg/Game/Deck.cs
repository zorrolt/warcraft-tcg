﻿using System;
using System.Collections.Generic;
using System.Linq;
using WarTcg.Game.Cards;

namespace WarTcg.Game
{
    public class Deck
    {
        private readonly Random _rnd;
        public Deck()
        {
            _rnd = new Random();
            Cards = new Stack<Card>();
        }

        public void Shuffle()
        {
            var values = Cards.ToArray();
            Cards.Clear();
            foreach (var value in values.OrderBy(x => _rnd.Next()))
                Cards.Push(value);
        }

        public Guid Draw()
        {
            return Cards.Pop().Id;
        }

        public Stack<Card> Cards;
    }
}