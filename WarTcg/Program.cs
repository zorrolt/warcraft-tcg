﻿using EngineWar;
using WarTcg.Game;
using WarTcg.Menu;

namespace WarTcg
{
    internal class Program
    {
        private static void Main()
        {
            // Events
            MessageHandler.RegisterMessage(GlobalVariables.ToPauseMenu);
            MessageHandler.RegisterMessage(GlobalVariables.GameIsLoaded);

            var menuScreen = new MenuScreen();
            ScreenManager.Instance.AddScreen(menuScreen);
            ScreenManager.Instance.SetScreen(GlobalVariables.MenuScreenName);
            
            MessageHandler.SubscribeMessage(GlobalVariables.ToPauseMenu, (sender, args) =>
            {
                menuScreen.ToMenu();
                ScreenManager.Instance.SetScreen(GlobalVariables.MenuScreenName);
            });

            var gameScreen = new GameScreen();

            ScreenManager.Instance.AddScreen(gameScreen);

            //TODO: implement multiplayer
            var singlePlayerGameManager = new SinglePlayerGameManager();

            var game = new Engine();
            game.Run();
        }
    }
}
